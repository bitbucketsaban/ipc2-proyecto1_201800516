﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Supervisor.Master" AutoEventWireup="true" CodeBehind="RegistroEmpleadoS.aspx.cs" Inherits="Empresa.RegistroEmpleadoS" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h3 style="align-items: center">Registrar un Nuevo Empleado</h3>
    <br />
    <section class="container">
        <div class="row align-items-center">
            <div class="col-md-6  ">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="form-group">
                            <label>Nombres</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="Names" runat="server" CssClass="form-control" placeholder="Jorge Josue" required="required"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>Apellidos</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="LastName" runat="server" CssClass="form-control" placeholder="Perez Gomez" required="required"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>Fecha de Nacimiento</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="DateBeborn" runat="server" CssClass="form-control" placeholder="01/06/1995" required="required"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>NIT</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="NIT" runat="server" CssClass="form-control" placeholder="123455-6" required="required"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>Correo</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="Correo" runat="server" CssClass="form-control" placeholder="ejemlo@gmail.com"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>Contraseña</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="Contraseña" runat="server" CssClass="form-control" placeholder="*********" required="required"></asp:TextBox>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-md-6 ">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="form-group">
                            <label>Domicilio</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="Direccion" runat="server" CssClass="form-control" placeholder="Guatemala, Ciudad Capital , Zona 8 5-5" required="required"></asp:TextBox>
                        </div>

                        <div class="form-group">
                            <label>Telefono de casa</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="HomePhone" runat="server" CssClass="form-control" placeholder="21457852"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>Telefono</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="Celphone" runat="server" CssClass="form-control" placeholder="12045782" required="required"></asp:TextBox>
                        </div>

                        <br />
                        <div class="form-group">
                            <label>Puesto a desempeñar</label>
                        </div>
                        <div class="form-group" aria-checked="true">

                            <asp:RadioButton ID="Radio1" Text=".  Vendedor" Checked="false" GroupName="RadioGroup1" runat="server" AutoPostBack="True" OnCheckedChanged="Radio1_CheckedChanged" /><br />
                            <asp:RadioButton ID="RadioButton1" Text=".  Supervisor" Checked="False" GroupName="RadioGroup1" runat="server" AutoPostBack="True" OnCheckedChanged="RadioButton2_CheckedChanged" /><br />
                            <asp:RadioButton ID="RadioButton2" Text=".  Gerente" Checked="False" GroupName="RadioGroup1" runat="server" AutoPostBack="True" OnCheckedChanged="RadioButton3_CheckedChanged" /><br />
                        </div>
                        <div class="form-group">
                            <label>Jefe</label>
                        </div>
                        <asp:Label runat="server" ID="label"></asp:Label>
                        <div class="form-group">

                            <asp:DropDownList runat="server" ID="jefes"></asp:DropDownList>
                        </div>

                    </div>
                </div>
            </div>

        </div>



        <div class="row align-items-center">
            <div>



                <div runat="server" id="theDiv">

                    <h3 style="text-align: center">Asignando Vehiculos a Empleados</h3>

                    <div>
                        <b></b>
                        <br />
                        <h5 style="text-align: center; color: red">Vehiculos Disponibles</h5>

                        <asp:GridView ID="GridView1" HeaderStyle-BackColor="#3AC0F2" HeaderStyle-ForeColor="White"
                            runat="server" AutoGenerateColumns="False" align="center">
                            <Columns>
                                <asp:BoundField DataField="Placa" HeaderText="Placas" ItemStyle-BackColor="Yellow">
                                    <ItemStyle BackColor="Yellow"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="NumeroDeMotor" HeaderText="Numero De Motor" ItemStyle-BackColor="Yellow">
                                    <ItemStyle BackColor="Yellow"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="NumeroDeChasis" HeaderText="Numero De Chasis" ItemStyle-BackColor="Yellow">
                                    <ItemStyle BackColor="Yellow"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Marca" HeaderText="Marca" ItemStyle-BackColor="Yellow">
                                    <ItemStyle BackColor="Yellow"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Modelo" HeaderText="Modelo" ItemStyle-BackColor="Yellow">
                                    <ItemStyle BackColor="Yellow"></ItemStyle>
                                </asp:BoundField>
                                <asp:BoundField DataField="Estilo" HeaderText="Estilo" ItemStyle-BackColor="Yellow">

                                    <ItemStyle BackColor="Yellow"></ItemStyle>
                                </asp:BoundField>


                            </Columns>

                            <HeaderStyle BackColor="#3AC0F2" ForeColor="White"></HeaderStyle>
                        </asp:GridView>
                        <br />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Label Text="Digite Placas" runat="server" />
                        &nbsp;&nbsp;&nbsp;
                            <asp:TextBox runat="server" ID="placaquita" request="request" />

                    </div>
                </div>



                <asp:Button ID="Registro" runat="server" Height="31px" Text="Registrar Empleado" Width="171px" class="btn btn-primary btn-block btn-large" OnClick="Registro_Click" />

                <asp:Button ID="carro" runat="server" Height="31px" Text="Agregar Vehiculo" Width="171px" class="btn btn-danger btn-block btn-large" OnClick="carro_Click" />


            </div>




        </div>

    </section>

    <div runat="server" id="nuevocarro">
        <div class="col-md-6  ">
            <div class="box box-primary">
                <div class="box-body">
                    <div class="form-group">
                        <label>Numero de Placa</label>
                    </div>
                    <div class="form-group">
                        <asp:TextBox ID="Nplaca" runat="server" CssClass="form-control" placeholder="4554" required="required"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Numero de Motor</label>
                    </div>
                    <div class="form-group">
                        <asp:TextBox ID="Nmotor" runat="server" CssClass="form-control" placeholder="0001111" required="required"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Numero de Chasis</label>
                    </div>
                    <div class="form-group">
                        <asp:TextBox ID="Nchasis" runat="server" CssClass="form-control" placeholder="000001111212" required="required"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Marca</label>
                    </div>
                    <div class="form-group">
                        <asp:TextBox ID="Marca" runat="server" CssClass="form-control" placeholder="Mazda" required="required"></asp:TextBox>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-6  ">
            <div class="box box-primary">
                <div class="box-body">
                    <div class="form-group">
                        <label>Modelo</label>
                    </div>
                    <div class="form-group">
                        <asp:TextBox ID="Modelo" runat="server" CssClass="form-control" placeholder="1995"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Estilo</label>
                    </div>
                    <div class="form-group">
                        <asp:TextBox ID="Estilo" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <br />
                <asp:Button ID="crearvehiculo" runat="server" Height="31px" Text="Nuevo Vehiculo" Width="171px" class="btn btn-danger btn-block btn-large" OnClick="crearvehiculo_Click" />
            </div>
        </div>

    </div>




    <style>
        body{
            background-color:moccasin;
        }
        button {
            background: #3da4ab;
            color: azure;
            display: block;
    
            padding: 10px;
        }
            /* para darle accion al boton como cambio de color cuando este el mouse sobre se usa hover */

            button:hover {
                background: red;
            }
            /* cuando ya se ha seleccionado el boton  */

            button:active {
                color: yellow;
            }
    </style>






</asp:Content>
