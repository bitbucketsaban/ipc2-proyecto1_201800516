﻿<%@ Page Title="Crear_Producto" Language="C#" MasterPageFile="~/Administrador.Master" AutoEventWireup="true" CodeBehind="Crear_Producto.aspx.cs" Inherits="Empresa.WebForm2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <section class="container-header">
        <h3 style="text-align: center; font: bold 25 px" >Agregar Nuevo Producto</h3>
        <br />
        <br />
        <asp:Label ID="prueba" runat="server"></asp:Label>

    </section>
    <section class="container">
        <div class="row align-items-center">
            <div class="col-md-6  ">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="form-group">
                            <label>Codigo del Producto</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="IdProduct" runat="server" CssClass="form-control" placeholder="Codigo De Producto" required="required"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>Selecciona:<br /></label>
                        </div>
                        <div class="form-group">
                            <asp:DropDownList  runat="server" ID="Categoria"  >

                                

                            </asp:DropDownList>
                        </div>
                        <div class="form-group">
                            <label>Nombre de Producto</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="NameProduct" runat="server" CssClass="form-control" placeholder="Arroz" required="required"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>Descripcion</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="Description" runat="server" CssClass="form-control" placeholder="1 Lb" required="required"></asp:TextBox>
                        </div>
                       
                    </div>
                </div>
            </div>

            <div class="col-md-6  ">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="form-group">
                            <label>Comentario</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="Comment" runat="server" CssClass="form-control" placeholder="Arroz Precosido"></asp:TextBox>

                        </div>
                        <div class="form-group">
                            <label>Imagen</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="Imagen" runat="server" CssClass="form-control" placeholder="Imagenes/Producto1.png"></asp:TextBox>

                        </div>
                        <div class="form-group">
                            <label>Id de la lista de Precios</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="listaPrecio" runat="server" CssClass="form-control" placeholder="010"></asp:TextBox>

                        </div>
                       

                    </div>
                </div>
            </div>



            



        </div>
        <asp:Button ID="CreateProduct" runat="server" Text="Crear Producto Nuevo" Height="51px" Width="185px" OnClick="CreateProduct_Click"  />
      
    </section>

    <style>
        #CreateProduct {
            background: #3da4ab;
            color: azure;
            display: block;
            align-content:center;
            align-items:center;
            padding: 10px;
        }
            /* para darle accion al boton como cambio de color cuando este el mouse sobre se usa hover */

            #CreateProduct:hover {
                background: red;
            }
            /* cuando ya se ha seleccionado el boton  */

            #CreateProduct:active {
                color: yellow;
            }
            body{
                background-color:skyblue;
            }
    </style>


</asp:Content>
