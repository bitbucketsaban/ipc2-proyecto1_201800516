﻿<%@ Page Title="Nueva Categoria" Language="C#" MasterPageFile="~/Administrador.Master" AutoEventWireup="true" CodeBehind="Crear_Categoria.aspx.cs" Inherits="Empresa.WebForm4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <section class="container-header">
        <h3 style="text-align: center; font: bold 25 px">Agregar Nueva Categoria</h3>
        <br />
        <br />

    </section>
    <section class="container align-items-center">
        <div class="row align-items-center">
            <div class="col-md-3">

            </div>
            <div class="col-md-6  ">
                <div class="box box-primary">
                    <div class="box-body">
                        <div class="form-group">
                            <label>Codigo de la Categoria</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="IdCategory" runat="server" CssClass="form-control" placeholder="Codigo de la Categoria" required="required"></asp:TextBox>
                        </div>
                        
                        </div>
                        <div class="form-group">
                            <label>Nombre de la Categoria</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="NameCategory" runat="server" CssClass="form-control" placeholder="Joyeria" required="required"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label>Descripcion</label>
                        </div>
                        <div class="form-group">
                            <asp:TextBox ID="Description" runat="server" CssClass="form-control" placeholder=" Descripcion de la Categoria" required="required"></asp:TextBox>
                        </div>
                        
                        
                    
                </div>
            </div>

            <div class="col md-3 align-self-center">
                <asp:Button ID="CreateCategory" runat="server" Text="Crear Nueva Categoria" Height="51px" Width="185px" OnClick="CreateCategory_Click" />
            </div>

        </div>

        

    </section>

    <style>
        #CreateProduct {
            background: #3da4ab;
            color: azure;
            display: block;
            align-content:center;
            align-items:center;
            padding: 10px;
        }
            /* para darle accion al boton como cambio de color cuando este el mouse sobre se usa hover */

            #CreateProduct:hover {
                background: red;
            }
            /* cuando ya se ha seleccionado el boton  */

            #CreateProduct:active {
                color: yellow;
            }
            body{
                background-color:skyblue;
            }
    </style>


</asp:Content>
