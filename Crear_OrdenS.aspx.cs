﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;



namespace Empresa
{
    public partial class Crear_OrdenS : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            editor.Visible = false;

            datos.Visible = false;
            datos2.Visible = false;

        }

        public void llenadoTabla()
        {
            string connectionString = @"Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                sqlCon.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter("select c.Codigo_Producto, c.Nombre,p.Precio,c.Descripcion from Producto as c join DetalleLista as p on c.Codigo_Producto=p.CodigoProducto join Categoria as cat on c.IdCategoria=cat.Codigo where c.Codigo_Producto=p.CodigoProducto and cat.Nombre='" + Categoria.SelectedItem.Text + "'", sqlCon);
                DataTable dtbl = new DataTable();
                sqlDa.Fill(dtbl);
                GridView1.DataSource = dtbl;
                GridView1.DataBind();

            }
        }
        protected void Categoria_SelectedIndexChanged(object sender, EventArgs e)
        {

            llenadoTabla();

        }
        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView1.EditIndex = -1;
            GridView1.PageIndex = e.NewPageIndex;
            llenadoTabla();
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {

            string t = GridView1.SelectedRow.Cells[2].ToString();
            label.Text = t;
        }

        protected void CreateProduct_Click(object sender, EventArgs e)

        {
            string connectionString = @"Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                int d = Int32.Parse(IdOrdene.Text);
                int p = Int32.Parse(nitempleado.Text);
                int pa = Int32.Parse(nitaprobacion.Text);

                DateTime pp = DateTime.Parse(fechaorden1.Text);
                DateTime aa = DateTime.Parse(aprobacion.Text);


                sqlCon.Open();
                if (Radio1.Checked == true)
                {
                    string s = "Aprobada";
                    SqlDataAdapter sqlDa = new SqlDataAdapter("INSERT INTO Orden VALUES (" + d + "," + TextBox1.Text + "," + p + ",'" + pp + "','" + aa + "'," + pa + ",'" + s + "')", sqlCon);
                    DataTable dtbl = new DataTable();

                    sqlDa.Fill(dtbl);
                    sqlCon.Close();
                }
                if (RadioButton1.Checked == true)
                {

                    string s = "No Aprobada";
                    SqlDataAdapter sqlDa = new SqlDataAdapter("INSERT INTO Orden VALUES (" + d + "," + TextBox1.Text + "," + p + ",'" + pp + "','" + aa + "'," + pa + ",'" + s + "')", sqlCon);
                    DataTable dtbl = new DataTable();

                    sqlDa.Fill(dtbl);
                    sqlCon.Close();
                }

                registro.Visible = false;

            }

        }

        public void ObtenerNit()
        {
            SqlConnection conexion = new SqlConnection();
            conexion.ConnectionString = "Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";
            conexion.Open();
            SqlCommand cmd = new SqlCommand("SELECT* FROM Cliente WHERE NIT='" + TextBox1.Text + "'", conexion);

            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read() == true)
            {

                datos.Visible = false;
                datos2.Visible = false;

                nom.Text = dr["Nombre"].ToString();
                apell.Text = dr["Apellido"].ToString();
                telef.Text = dr["Celular"].ToString();

                conexion.Close();
            }
            else
            {
                Response.Write("<script>alert('NIT NO REGISTRADO Registrar nit')</script>");
                datos.Visible = true;
                datos2.Visible = true;
                buscadornit.Visible = false;

            }


        }

        protected void buscaNit_Click(object sender, EventArgs e)
        {

            ObtenerNit();

        }

        protected void prueba(object sender, EventArgs e)
        {
            label.Text = GridView1.SelectedRow.Cells[1].Text;

            DataTable dtbl = new DataTable();
            dtbl.Columns.Add("Codigo");
            dtbl.Columns.Add("Nombre");
            Session["dtbl"] = dtbl;
            dtbl.Rows.Add(label.Text, "hola");
            GridView2.DataSource = dtbl;

        }

        protected void registrar_Click(object sender, EventArgs e)
        {
            string connectionString = @"Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                sqlCon.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter("INSERT INTO Cliente(NIT,Nombre, Apellido,Nacimiento,Direccion,Telefono,Celular,Email,IdCiudad,Limite_Credito,Dias_Credito) values('" + NIT.Text + "', '" + Names.Text + "', '" + LastName.Text + "', '" + DateBeborn.Text + "', '" + Direccion.Text + "', '" + HomePhone.Text + "', '" + Celphone.Text + "', '" + Correo.Text + "', 1, '" + Limite.Text + "', '" + credito.Text + "')"
                , sqlCon);
                DataTable dtbl = new DataTable();

                sqlDa.Fill(dtbl);
                sqlCon.Close();

                registro.Visible = false;

            }

        }

        protected void editar_Click(object sender, EventArgs e)
        {
            editor.Visible = true;

        }

        protected void Agregar_Click(object sender, EventArgs e)
        {
            SqlConnection sqlCon = new SqlConnection(@"Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;");


            SqlCommand comando = new SqlCommand("select* from Orden", sqlCon);
            sqlCon.Open();
            SqlDataReader Registro = comando.ExecuteReader();
            while (Registro.Read())
            {
                String Nit_Cliente = (Registro["Nit_Cliente"].ToString());
                String Numero_Orden = (Registro["Numero_Orden"].ToString());

                if (IdOrdene.Text.Equals(Numero_Orden) && TextBox1.Text.Equals(Nit_Cliente))
                {
                    acarro();
                    total.Text = calculototal().ToString();
                }
                else
                {
                    Response.Write("<script>alert('No existe la orden o cliente')</script>");
                }

            }
            sqlCon.Close();

        }
        public void acarro()
        {
            string connectionString = @"Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                int orden = Int32.Parse(IdOrdene.Text);
                int product = Int32.Parse(Producto.Text);
                int d = 1;

                double precio = ObtenerProducto(product);
                double total = d * precio;

                sqlCon.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter("INSERT INTO DetalleOrden(NumeroOrden,Codigo_Producto_Ordenado,Cantidad,Total) VALUES(" + orden + "," + product + ",1," + total + ")", sqlCon);
                DataTable dtbl = new DataTable();

                sqlDa.Fill(dtbl);
                sqlCon.Close();

                registro.Visible = false;
                carrito();
            }
        }
        public void carrito()
        {
            string connectionString = @"Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                int d = Int32.Parse(IdOrdene.Text);
                sqlCon.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter("select Producto.Nombre,DetalleOrden.Cantidad,DetalleOrden.Total FROM Producto join DetalleOrden ON DetalleOrden.Codigo_Producto_Ordenado=Producto.Codigo_Producto where DetalleOrden.NumeroOrden=" + d + "", sqlCon);
                DataTable dtbl = new DataTable();
                sqlDa.Fill(dtbl);
                GridView2.DataSource = dtbl;
                GridView2.DataBind();

            }
        }

        public double ObtenerProducto(int d)
        {
            double doble = 0;
            SqlConnection conexion = new SqlConnection();
            conexion.ConnectionString = "Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";
            conexion.Open();
            SqlCommand cmd = new SqlCommand("select p.Precio as p from Producto as c join DetalleLista as p on c.Codigo_Producto=p.CodigoProducto join Categoria as cat on c.IdCategoria=cat.Codigo where c.Codigo_Producto=p.CodigoProducto and c.Codigo_Producto= " + d + ";", conexion);

            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read() == true)
            {



                doble = double.Parse(dr["p"].ToString());

                conexion.Close();
            }
            else
            {
                Response.Write("<script>alert('NIT NO REGISTRADO Registrar nit')</script>");
                datos.Visible = true;
                datos2.Visible = true;
                buscadornit.Visible = false;

            }
            return doble;

        }

        protected void actualizacion_Click(object sender, EventArgs e)
        {
            string connectionString = @"Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "agregaorden";
                cmd.Parameters.Add("@Codigo", SqlDbType.BigInt).Value = Producto.Text;
                cmd.Parameters.Add("@orden", SqlDbType.BigInt).Value = IdOrdene.Text;
                cmd.Parameters.Add("@cant", SqlDbType.BigInt).Value = Cantidad.Text;
                cmd.Parameters.Add("@precio", SqlDbType.Decimal).Value = ObtenerProducto(Int32.Parse(Producto.Text));
                cmd.Connection = sqlCon;
                sqlCon.Open();
                cmd.ExecuteNonQuery();



                Response.Write("<script>alert('Se agrego su producto')</script>");

                sqlCon.Close();
                carrito();
                total.Text = calculototal().ToString();

            }
        }

        protected void eliminar(object sender, EventArgs e)
        {
            string connectionString = @"Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "Pedidomenos";
                cmd.Parameters.Add("@Codigo", SqlDbType.BigInt).Value = Producto.Text;
                cmd.Parameters.Add("@orden", SqlDbType.BigInt).Value = IdOrdene.Text;
                cmd.Parameters.Add("@cant", SqlDbType.BigInt).Value = Cantidad.Text;
                cmd.Parameters.Add("@precio", SqlDbType.Decimal).Value = ObtenerProducto(Int32.Parse(Producto.Text));
                cmd.Connection = sqlCon;
                sqlCon.Open();
                cmd.ExecuteNonQuery();



                Response.Write("<script>alert('Se elimino el producto')</script>");

                sqlCon.Close();
                carrito();
                total.Text = calculototal().ToString();


            }
        }

        protected void GridView2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        public double calculototal()
        {
            double doble = 0;
            SqlConnection conexion = new SqlConnection();
            conexion.ConnectionString = "Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";
            conexion.Open();
            SqlCommand cmd = new SqlCommand("select sum(Total) as Total from DetalleOrden", conexion);

            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read() == true)
            {

                doble = double.Parse(dr["Total"].ToString());

                conexion.Close();
            }

            return doble;

        }

    }
}