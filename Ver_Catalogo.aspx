﻿<%@ Page Title="Catalogo" Language="C#" MasterPageFile="~/Administrador.Master" AutoEventWireup="true" CodeBehind="Ver_Catalogo.aspx.cs" Inherits="Empresa.WebForm3" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <!DOCTYPE html>
    
    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Empleados</title>
        <style type="text/css">
            .header {
                height: 16px;
                width: 25px;
                background-color:bisque;
            }
        </style>
    </head>
    <body>

        <h3 style="text-align: center">Catalogo De Productos</h3>

        <div class="col-m-6" >
            <div>
                <b></b>
                <br />
                <asp:GridView ID="GridView1" HeaderStyle-BackColor="#3AC0F2" HeaderStyle-ForeColor="White"
                    runat="server" AutoGenerateColumns="False" align="center">
                    <Columns>
                        <asp:BoundField DataField="Codigo" HeaderText="Codigo" ItemStyle-BackColor="Yellow">
                            <ItemStyle BackColor="Yellow"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="Nombre" HeaderText="Nombre" ItemStyle-BackColor="Yellow">
                            <ItemStyle BackColor="Yellow"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="Descripcion" HeaderText="Descripcion" ItemStyle-BackColor="Yellow">


                            <ItemStyle BackColor="Yellow"></ItemStyle>
                        </asp:BoundField>

                    </Columns>

                    <HeaderStyle BackColor="#3AC0F2" ForeColor="White"></HeaderStyle>
                </asp:GridView>
            </div>
        </div>
        <div class="col-m-6">
            <div>
                <b></b>
                <br />
                <asp:Label ID="LA" runat="server" Text="Ingrese el Id de Categoria"/>
                <br />
                <br />
                <asp:TextBox ID="CODIGO" runat="server"/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="Eliminar" runat="server" Text="Eliminar Categoria" OnClick="Eliminar_Click" />
                &nbsp;
                <asp:Button ID="Editar" runat="server" Text="Editar Categoria" OnClick="Editar_Click"  />

                <br />

            </div>
            <div id="Edicion" runat="server">

            
            <div class="form-group">
                <label>Nombre de la Categoria</label>
            </div>
            <div class="form-group">
                <asp:TextBox ID="NameCategory" runat="server" CssClass="form-control" placeholder="Joyeria" required="required"></asp:TextBox>
            </div>
            <div class="form-group">
                <label>Descripcion</label>
            </div>
            <div class="form-group">
                <asp:TextBox ID="Description" runat="server" CssClass="form-control" placeholder=" Descripcion de la Categoria" ></asp:TextBox>
            </div>
            <br />
            <asp:Button ID="Actualizar" runat="server" Text="Actualizar" OnClick="Actualizar_Click" />
            </div>
      
        </div>
    </body>
    </html>

</asp:Content>
