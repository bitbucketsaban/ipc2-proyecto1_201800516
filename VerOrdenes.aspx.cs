﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Security.Policy;
using System.Configuration.Internal;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Xml.Linq;
using Org.BouncyCastle.Bcpg.OpenPgp;
using System.Windows.Forms;

namespace Empresa
{
    public partial class VerOrdenes : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            tabla();
            llenadomoneda();
            int dato = porden.Text.Length;
            if (dato > 0)
            {
                pago.Visible = true;
                string nombre = pagos.SelectedValue.ToString();

                if (nombre.Equals("1"))
                {
                    cheque.Visible = false;
                    efectivo.Visible = true;
                    tarjeta.Visible = false;
                }
                else if (nombre.Equals("2"))
                {
                    cheque.Visible = false;
                    efectivo.Visible = false;
                    tarjeta.Visible = true;
                }
                else if (nombre.Equals("3"))
                {
                    cheque.Visible = true;
                    efectivo.Visible = false;
                    tarjeta.Visible = false;
                }
            }
            else
            {
                cheque.Visible = false;
                efectivo.Visible = false;
                tarjeta.Visible = false;
            }

        }

        public void tabla()
        {
            string connectionString = @"Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {

                sqlCon.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter("select * from Orden", sqlCon);
                DataTable dtbl = new DataTable();
                sqlDa.Fill(dtbl);
                GridView1.DataSource = dtbl;
                GridView1.DataBind();

            }
        }



        protected void Orden_Click(object sender, EventArgs e)
        {
            
        }

        protected void cobrar_Click(object sender, EventArgs e)
        {

            if (aprobacion().Equals("Aprobada"))
            {
                pago.Visible = true;
                pagorden();
            }
            else
            {
                Response.Write("<script>alert('Orden no esta Aprobada para pagar')</script>");
            }
        }

        /// Hola que tal, me entere de la noticia de tu papa, pero lo unico 



        public void llenadomoneda()

        {
            SqlConnection conexion = new SqlConnection();
            conexion.ConnectionString = "Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";

            SqlCommand comando = new SqlCommand("select * from Moneda", conexion);
            conexion.Open();
            SqlDataReader Registro = comando.ExecuteReader();
            while (Registro.Read())
            {
                tiposmonedas.Items.Add(Registro["NombreM"].ToString());
                monedas2.Items.Add(Registro["NombreM"].ToString());
                monedas3.Items.Add(Registro["NombreM"].ToString());



            }
            conexion.Close();
        }

        protected void tiposmonedas_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlConnection conexion = new SqlConnection();
            conexion.ConnectionString = "Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";
            conexion.Open();
            SqlCommand cmd = new SqlCommand("SELECT* FROM Moneda where NombreM='" + tiposmonedas.SelectedValue.ToString() + "'", conexion);

            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read() == true)
            {
                double precio = double.Parse(deuda.Text) / double.Parse(dr["Taza"].ToString());
                simbolo.Text = dr["NombreM"].ToString();
                conversion.Text = precio.ToString();
                label3.Text = dr["NombreM"].ToString();
                label4.Text = precio.ToString();
                label1.Text = dr["NombreM"].ToString();
                label2.Text = precio.ToString();
                conexion.Close();
            }
        }

        protected void pcheque_Click(object sender, EventArgs e)
        {
            string connectionString = @"Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";
            double pp = double.Parse(TextBox2.Text);

            if ((pp * tasa(monedas3.SelectedValue.ToString())) <= double.Parse(deuda.Text))
            {
                using (SqlConnection sqlCon = new SqlConnection(connectionString))
                {

                    string pago = "Cheque";
                    sqlCon.Open();
                    int d = Int32.Parse(porden.Text);
                    double pagodolar = pp * tasa(monedas3.SelectedValue.ToString());
                    double pagopendietne = double.Parse(deuda.Text) - pagodolar;
                    SqlDataAdapter sqlDa = new SqlDataAdapter("INSERT INTO pagos VALUES (" + d + ",'" + pago + "'," + pagodolar + ",'" + label3.Text + "','" + tasa(monedas3.SelectedValue.ToString()).ToString() + "','" + DateTime.Parse(fechapago.Text) + "')", sqlCon);
                    DataTable dtbl = new DataTable();

                    sqlDa.Fill(dtbl);
                    sqlCon.Close();
                }

                actualizaciondeuda(pp, monedas3.SelectedValue.ToString());

            }
            else
            {
                Response.Write("<script>alert('Esta pagando mas')</script>");
            }





        }

        protected void ptarjeta_Click(object sender, EventArgs e)
        {
            string connectionString = @"Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";
            double pp = double.Parse(TextBox1.Text);



            if ((pp * tasa(monedas2.SelectedValue.ToString())) <= double.Parse(deuda.Text))
            {
                using (SqlConnection sqlCon = new SqlConnection(connectionString))
                {

                    string pago = "Tarjeta";
                    sqlCon.Open();
                    int d = Int32.Parse(porden.Text);
                    double pagodolar = pp * tasa(monedas2.SelectedValue.ToString());
                    double pagopendietne = double.Parse(deuda.Text) - pagodolar;
                    SqlDataAdapter sqlDa = new SqlDataAdapter("INSERT INTO pagos VALUES (" + d + ",'" + pago + "'," + pagodolar + ",'" + label1.Text + "',' " + tasa(monedas2.SelectedValue.ToString()) + "','" + DateTime.Parse(fechapago.Text) + "')", sqlCon);
                    DataTable dtbl = new DataTable();

                    sqlDa.Fill(dtbl);
                    sqlCon.Close();
                }
                actualizaciondeuda(pp, monedas2.SelectedValue.ToString());
            }
            else
            {
                Response.Write("<script>alert('Esta pagando mas')</script>");
            }






        }

        protected void pefectivo_Click(object sender, EventArgs e)
        {
            double pp = double.Parse(pagoefectivo.Text);


            if ((pp * tasa(tiposmonedas.SelectedValue.ToString())) <= double.Parse(deuda.Text))
            {
                string connectionString = @"Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";

                using (SqlConnection sqlCon = new SqlConnection(connectionString))
                {

                    string pago = "Efectivo";
                    sqlCon.Open();
                    int d = Int32.Parse(porden.Text);
                    double pagodolar = pp * tasa(tiposmonedas.SelectedValue.ToString());
                    double pagopendietne = double.Parse(deuda.Text) - pagodolar;
                    SqlDataAdapter sqlDa = new SqlDataAdapter("INSERT INTO pagos VALUES (" + d + ",'" + pago + "'," + pagodolar + ",'" + simbolo.Text + "','" + tasa(tiposmonedas.SelectedValue.ToString()) + "','" + DateTime.Parse(fechapago.Text) + "')", sqlCon);
                    DataTable dtbl = new DataTable();

                    sqlDa.Fill(dtbl);
                    sqlCon.Close();
                }
                actualizaciondeuda(pp, tiposmonedas.SelectedValue.ToString());
            }
            else
            {
                Response.Write("<script>alert('Esta pagando mas')</script>");
            }
        }

        protected void pagos_SelectedIndexChanged(object sender, EventArgs e)
        {

        }




        public void pagorden()
        {
            SqlConnection conexion = new SqlConnection();
            conexion.ConnectionString = "Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";

            SqlCommand comando = new SqlCommand("select * from Orden where Numero_Orden=" + Int32.Parse(porden.Text), conexion);
            conexion.Open();
            SqlDataReader Registro = comando.ExecuteReader();
            while (Registro.Read())
            {

                deuda.Text = Registro["Total"].ToString();

            }

            conexion.Close();
        }


        public void actualizaciondeuda(double dinero, string combo)
        {
            string connectionString = @"Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "restardeuda";
                cmd.Parameters.Add("@orden", SqlDbType.BigInt).Value = Int32.Parse(porden.Text);
                double pago = dinero * tasa(combo);
                cmd.Parameters.Add("@abono", SqlDbType.Decimal).Value = pago;
                cmd.Connection = sqlCon;
                sqlCon.Open();
                cmd.ExecuteNonQuery();
            }

        }


        public double tasa(string combo)
        {
            double tasa = 0;

            SqlConnection conexion = new SqlConnection();
            conexion.ConnectionString = "Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";
            conexion.Open();
            SqlCommand cmd = new SqlCommand("SELECT* FROM Moneda where NombreM='" + combo + "'", conexion);

            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read() == true)
            {
                tasa = double.Parse(dr["Taza"].ToString());


            }
            return tasa;
        }

        public string aprobacion()
        {
            string tasa = "";

            SqlConnection conexion = new SqlConnection();
            conexion.ConnectionString = "Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";
            conexion.Open();
            SqlCommand cmd = new SqlCommand("SELECT* FROM Orden where Numero_Orden=" + Int32.Parse(porden.Text) + "", conexion);

            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read() == true)
            {
                tasa = dr["Aprobacion"].ToString();


            }
            return tasa;
        }

        protected void monedas2_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlConnection conexion = new SqlConnection();
            conexion.ConnectionString = "Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";
            conexion.Open();
            SqlCommand cmd = new SqlCommand("SELECT* FROM Moneda where NombreM='" + monedas2.SelectedValue.ToString() + "'", conexion);

            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read() == true)
            {
                double precio = double.Parse(deuda.Text) / double.Parse(dr["Taza"].ToString());
                simbolo.Text = dr["NombreM"].ToString();
                conversion.Text = precio.ToString();
                label3.Text = dr["NombreM"].ToString();
                label4.Text = precio.ToString();
                label1.Text = dr["NombreM"].ToString();
                label2.Text = precio.ToString();
                conexion.Close();
            }
        }

        protected void monedas3_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlConnection conexion = new SqlConnection();
            conexion.ConnectionString = "Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";
            conexion.Open();
            SqlCommand cmd = new SqlCommand("SELECT* FROM Moneda where NombreM='" + monedas3.SelectedValue.ToString() + "'", conexion);

            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read() == true)
            {
                double precio = double.Parse(deuda.Text) / double.Parse(dr["Taza"].ToString());
                simbolo.Text = dr["NombreM"].ToString();
                conversion.Text = precio.ToString();
                label3.Text = dr["NombreM"].ToString();
                label4.Text = precio.ToString();
                label1.Text = dr["NombreM"].ToString();
                label2.Text = precio.ToString();
                conexion.Close();
            }
        }




    }
}