﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using Microsoft.Ajax.Utilities;

namespace Empresa
{
    public partial class WebForm7 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Edicion.Visible = false;
            llenadoTabla();
        }

        public void llenadoTabla()
        {
            string connectionString = @"Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                sqlCon.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter("select E.NIT,E.Nombre,E.Apellido,E.Direccion,p.Nombre as Puesto,E.Email,E.Contraseña,E.Celular,E.Telefono, E.Codigo_Jefe as Jefe, v.Marca AS Marca_del_Vehiculo from Empleado as E LEFT join Vehiculo as v on E.PlacaVehiculo = v.Placa join Puesto as p on p.Codigo = E.Codigo_Puesto", sqlCon);

                DataTable dtbl = new DataTable();
                sqlDa.Fill(dtbl);
                GridView1.DataSource = dtbl;
                GridView1.DataBind();

            }
        }

        protected void Editar_Click(object sender, EventArgs e)
        {
            Edicion.Visible = true;

        }

        protected void Eliminar_Click(object sender, EventArgs e)
        {
            string connectionString = @"Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "eliminarempleado";
                cmd.Parameters.Add("@nit", SqlDbType.Text).Value = CODIGO.Text;
                cmd.Connection = sqlCon;
                sqlCon.Open();
                cmd.ExecuteNonQuery();



                Response.Write("<script>alert('Se Elimino El Empleado')</script>");

                sqlCon.Close();
                llenadoTabla();


            }

        }

        protected void Actualizar_Click(object sender, EventArgs e)
        {
            string connectionString = @"Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "editarempleado";
                cmd.Parameters.AddWithValue("@correo", Correo.Text);
                cmd.Parameters.AddWithValue("@nit", CODIGO.Text);

                cmd.Parameters.AddWithValue("@contra", Contraseña.Text);

                cmd.Parameters.AddWithValue("@direccion", Direccion.Text);
                cmd.Parameters.AddWithValue("@telcasa", HomePhone.Text);
                cmd.Parameters.AddWithValue("@telefono", Celphone.Text);



                cmd.Connection = sqlCon;
                sqlCon.Open();
                cmd.ExecuteNonQuery();



                Response.Write("<script>alert('Se actualizo')</script>");

                sqlCon.Close();
                llenadoTabla();
            }
        }
    }
}