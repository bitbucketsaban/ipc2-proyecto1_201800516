﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace Empresa
{
    public partial class WebForm3 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            llenadoTabla();
            //if (!this.IsPostBack)
            //{
            //    DataTable dt = new DataTable();
            //    dt.Columns.AddRange(new DataColumn[3] { new DataColumn("Id", typeof(long)),
            //                new DataColumn("Nombre", typeof(string)),
            //                new DataColumn("Caracteristic",typeof(string))});
            //    dt.Rows.Add(1201, "Electrodomestico", "");
            //    dt.Rows.Add(20120, "Mueble", " ");
            //    dt.Rows.Add(252105, "Joyeria",  "Distribuidora La Mejor");
            //    dt.Rows.Add(00824, "Electronico", " Apple");
            //    GridView1.DataSource = dt;
            //    GridView1.DataBind();

            //}
            Edicion.Visible = false;

        }


        public void llenadoTabla()
        {
            string connectionString = @"Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                sqlCon.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter("select * from Categoria", sqlCon);
                DataTable dtbl = new DataTable();
                sqlDa.Fill(dtbl);
                GridView1.DataSource = dtbl;
                GridView1.DataBind();

            }
        }

        protected void Eliminar_Click(object sender, EventArgs e)
        {
            int id = Int32.Parse(CODIGO.Text);

            string connectionString = @"Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "eliminacategoria";
                cmd.Parameters.Add("@codigocategoria", SqlDbType.BigInt).Value = id;
                cmd.Connection = sqlCon;
                sqlCon.Open();
                cmd.ExecuteNonQuery();



                Response.Write("<script>alert('Se Elimino)</script>");

                sqlCon.Close();
                llenadoTabla();


            }
        }

        protected void Actualizar_Click(object sender, EventArgs e)
        {
            int id = Int32.Parse(CODIGO.Text);

            string connectionString = @"Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "editarcategoria";
                cmd.Parameters.AddWithValue("@codigocategoria", id);

                cmd.Parameters.AddWithValue("@Nombre", NameCategory.Text);

                cmd.Parameters.AddWithValue("@Descripcion", Description.Text);

                cmd.Connection = sqlCon;
                sqlCon.Open();
                cmd.ExecuteNonQuery();



                Response.Write("<script>alert('Se actualizo')</script>");

                sqlCon.Close();
                llenadoTabla();
            }

        }

        protected void Editar_Click(object sender, EventArgs e)
        {
            Edicion.Visible = true;
        }
    }
}