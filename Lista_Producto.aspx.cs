﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;



namespace Empresa
{
    public partial class Lista_Producto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            SqlConnection sqlCon = new SqlConnection(@"Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;");


            llenadoTabla();
            combo.Items.Add("Seleccione");
            SqlCommand comando = new SqlCommand("select* from Producto", sqlCon);
            sqlCon.Open();
            SqlDataReader Registro = comando.ExecuteReader();
            while (Registro.Read())
            {
                combo.Items.Add(Registro["Nombre"].ToString());

            }
            sqlCon.Close();
            llenadoTablas();
        }
        public void llenadoTabla()
        {
            string connectionString = @"Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                sqlCon.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter("select l.IdLista,l.FechaInicial,l.FechaFinal,c.Nombre,p.Precio from Producto as c join DetalleLista as p on c.Codigo_Producto=p.CodigoProducto join ListaPrecio as l on p.IdListaD=l.IdLista", sqlCon);
                DataTable dtbl = new DataTable();
                sqlDa.Fill(dtbl);
                GridView1.DataSource = dtbl;
                GridView1.DataBind();

            }
        }

        public void llenadoTablas()
        {
            string connectionString = @"Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                sqlCon.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter("select * from ListaPrecio", sqlCon);
                DataTable dtbl = new DataTable();
                sqlDa.Fill(dtbl);
                GridView2.DataSource = dtbl;
                GridView2.DataBind();

            }
        }
        protected void agrega_Click(object sender, EventArgs e)
        {
            SqlConnection sqlCon = new SqlConnection(@"Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;");

            int cod = Int32.Parse(id.Text);
            double d = double.Parse(precio.Text);
            SqlDataAdapter sqlDa = new SqlDataAdapter("INSERT INTO DetalleLista VALUES (" + cod + "," + codigopro(combo.SelectedValue.ToString()) + "," + d + ");", sqlCon);

            DataTable dtbl = new DataTable();

            sqlDa.Fill(dtbl);
            sqlCon.Close();
            llenadoTabla();
        }
        public long codigopro(String nombre)
        {
            long valor = 0;
            SqlConnection sqlCon = new SqlConnection(@"Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;");

            sqlCon.Open();
            SqlCommand cmd = new SqlCommand("select * from Producto where Nombre='" + nombre + "'", sqlCon);

            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read() == true)
            {

                valor = Int32.Parse(dr["Codigo_Producto"].ToString());

                sqlCon.Close();
            }

            return valor;
        }
    }
}