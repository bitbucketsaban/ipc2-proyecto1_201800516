﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Administrador.Master" AutoEventWireup="true" CodeBehind="Lista_Producto.aspx.cs" Inherits="Empresa.Lista_Producto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h2 style="text-align: center">Lista de Precios con sus ordenes</h2>

    <asp:GridView ID="GridView1" HeaderStyle-BackColor="#3AC0F2" HeaderStyle-ForeColor="White"
        runat="server" AutoGenerateColumns="False" align="center">
        <Columns>
            <asp:BoundField DataField="IdLista" HeaderText="Codigo de Lista" ItemStyle-BackColor="Yellow">
                <ItemStyle BackColor="Yellow"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="FechaInicial" HeaderText="Fecha de Inicio" ItemStyle-BackColor="Yellow">
                <ItemStyle BackColor="Yellow"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="FechaFinal" HeaderText="Fecha Final " ItemStyle-BackColor="Yellow">
                <ItemStyle BackColor="Yellow"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="Nombre" HeaderText="Nombre del Producto" ItemStyle-BackColor="Yellow">
                <ItemStyle BackColor="Yellow"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="Precio" HeaderText="Precio" ItemStyle-BackColor="Yellow">
                <ItemStyle BackColor="Yellow"></ItemStyle>
            </asp:BoundField>


        </Columns>

        <HeaderStyle BackColor="#3AC0F2" ForeColor="White"></HeaderStyle>
    </asp:GridView>
    <br />
    <br />
    <asp:Label runat="server" Text="Ingrese el ID de la orden" />
    &nbsp;&nbsp;&nbsp;
    <asp:TextBox runat="server" ID="id" />
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:DropDownList runat="server" ID="combo">
    </asp:DropDownList>
    &nbsp;&nbsp;&nbsp;
    <asp:Label runat="server" Text="Precio" />

    &nbsp;&nbsp;&nbsp;
    <asp:TextBox runat="server" ID="precio" />
    &nbsp;&nbsp;&nbsp;
    <asp:Button ID="agrega" Text="Agregar" runat="server" OnClick="agrega_Click" />
    <br />
    <br />
    <h3 style="text-align:center"> Cambiar Vigencia </h3>

    <br />
    <br />



    <h2 style="text-align: center">LISTA DE PRECIOS</h2>

    <asp:GridView ID="GridView2" HeaderStyle-BackColor="#3AC0F2" HeaderStyle-ForeColor="White"
        runat="server" AutoGenerateColumns="False" align="center">
        <Columns>
            <asp:BoundField DataField="IdLista" HeaderText="Codigo de Lista" ItemStyle-BackColor="Yellow">
                <ItemStyle BackColor="Yellow"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="FechaInicial" HeaderText="Fecha de Inicio" ItemStyle-BackColor="Yellow">
                <ItemStyle BackColor="Yellow"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="FechaFinal" HeaderText="Fecha Final " ItemStyle-BackColor="Yellow">
                <ItemStyle BackColor="Yellow"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="Descripcion" HeaderText="Descripcion" ItemStyle-BackColor="Yellow">
                <ItemStyle BackColor="Yellow"></ItemStyle>
            </asp:BoundField>
            <asp:BoundField DataField="Vigencia" HeaderText="Vigencia" ItemStyle-BackColor="Yellow">
                <ItemStyle BackColor="Yellow"></ItemStyle>
            </asp:BoundField>


        </Columns>

        <HeaderStyle BackColor="#3AC0F2" ForeColor="White"></HeaderStyle>
    </asp:GridView>
   


</asp:Content>
