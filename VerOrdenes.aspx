﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Empleado.Master" AutoEventWireup="true" CodeBehind="VerOrdenes.aspx.cs" Inherits="Empresa.VerOrdenes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <!DOCTYPE html>
    <html>
    <head>
        <title></title>
    </head>
    <body>

        <h2 style="text-align: center">Visualizaciones de Ordenes</h2>
        <br />
        <asp:GridView ID="GridView1" HeaderStyle-BackColor="#3AC0F2" HeaderStyle-ForeColor="White"
            runat="server" AutoGenerateColumns="False" align="center">
            <Columns>
                <asp:BoundField DataField="Numero_Orden" HeaderText="Codigo de Orden" ItemStyle-BackColor="Yellow">
                    <ItemStyle BackColor="Yellow"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="Nit_Cliente" HeaderText="Nit del Cliente" ItemStyle-BackColor="Yellow">
                    <ItemStyle BackColor="Yellow"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="Nit_Empleado" HeaderText="Nit del Empleado" ItemStyle-BackColor="Yellow">
                    <ItemStyle BackColor="Yellow"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="Fecha_Creacion_Orden" HeaderText="Creacion de la Orden" ItemStyle-BackColor="Yellow">
                    <ItemStyle BackColor="Yellow"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="Fecha_Aprobacion_Orden" HeaderText="Aprobacion de la Orden" ItemStyle-BackColor="Yellow">
                    <ItemStyle BackColor="Yellow"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="Nit_Persona_Aprobada" HeaderText="Nit de la Aprobacion" ItemStyle-BackColor="Yellow">
                    <ItemStyle BackColor="Yellow"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="Aprobacion" HeaderText="Estado de La Orden" ItemStyle-BackColor="Yellow">
                    <ItemStyle BackColor="Yellow"></ItemStyle>
                </asp:BoundField>
                <asp:BoundField DataField="Total" HeaderText="Deuda" ItemStyle-BackColor="Yellow">
                    <ItemStyle BackColor="Yellow"></ItemStyle>
                </asp:BoundField>

            </Columns>

            <HeaderStyle BackColor="#3AC0F2" ForeColor="White"></HeaderStyle>
        </asp:GridView>

        <div>
            <h3 style="text-align: center">Cobrar una Orden</h3>
            <br />
            <asp:Label runat="server" Text="Ingrese la Orden " />
            &nbsp;<asp:TextBox ID="porden" runat="server" />
            &nbsp;&nbsp;
             <asp:Button ID="cobrar" runat="server" Text="Cobrar" OnClick="cobrar_Click" />
            <br />
            <br />

            <div id="pago" runat="server">
                <asp:Label runat="server" Text="Forma de Pago" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:DropDownList ID="pagos" runat="server" AutoPostBack="True" OnSelectedIndexChanged="pagos_SelectedIndexChanged">
                    <asp:ListItem Text="Seleccione" Value="0" />

                    <asp:ListItem Text="Efectivo" Value="1" />
                    <asp:ListItem Text="Tarjeta" Value="2" />
                    <asp:ListItem Text="Cheque" Value="3" />
                </asp:DropDownList>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label runat="server" Text="Debe : $  " />
                &nbsp;&nbsp;
                <asp:Label runat="server" ID="deuda" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Label runat="server" Text="Ingrese la Fecha " />
                &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:TextBox ID="fechapago" runat="server" />
                &nbsp;&nbsp;&nbsp;&nbsp;
                <br />

            </div>
            <div id="cheque" runat="server">
                <h3>Pago con cheque</h3>
                <br />
                <asp:Label runat="server" Text="Ingrese el Banco " />
                &nbsp;&nbsp;<asp:TextBox ID="bancocheque" runat="server" />
                &nbsp;&nbsp;&nbsp; &nbsp;
                    <asp:Label runat="server" Text="Numero de Cuenta" />
                &nbsp;&nbsp;
                    &nbsp;<asp:TextBox ID="Cuentacheque" runat="server" />&nbsp;&nbsp; &nbsp;&nbsp;
                    <asp:Label runat="server" Text="Numero de Cheque" />
                &nbsp;&nbsp;
                    &nbsp;<asp:TextBox ID="Ncheque" runat="server" />
                &nbsp;&nbsp;
                <asp:Label runat="server" Text="Ingrese la Cantidad " />
                &nbsp;&nbsp;<asp:TextBox ID="TextBox2" runat="server" />
                &nbsp;&nbsp;&nbsp;
                    <asp:DropDownList ID="monedas3" runat="server" AutoPostBack="True" OnSelectedIndexChanged="monedas3_SelectedIndexChanged">
                        <asp:ListItem Text="Seleccione" Value="0" />
                    </asp:DropDownList>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label runat="server" ID="label3" Text="m" />
                &nbsp;
                    <asp:Label runat="server" ID="label4" Text="222" />
                &nbsp;&nbsp;
             <asp:Button ID="pcheque" runat="server" Text="Pagar" OnClick="pcheque_Click" />
            </div>
            <div id="tarjeta" runat="server">
                <h3>Pago con Tarjeta </h3>
                <br />
                <asp:Label runat="server" Text="Ingrese el Emisor " />
                &nbsp;&nbsp;<asp:TextBox ID="emisor" runat="server" />
                &nbsp;&nbsp;&nbsp; &nbsp;
                    <asp:Label runat="server" Text="Numero de Autorizacion" />
                &nbsp;&nbsp;<asp:TextBox ID="autorizacion" runat="server" />
                &nbsp;&nbsp;
                <asp:Label runat="server" Text="Ingrese la Cantidad " />
                &nbsp;&nbsp;<asp:TextBox ID="TextBox1" runat="server" />
                &nbsp;&nbsp;&nbsp;
                    <asp:DropDownList ID="monedas2" runat="server" AutoPostBack="True" OnSelectedIndexChanged="monedas2_SelectedIndexChanged">
                        <asp:ListItem Text="Seleccione" Value="0" />
                    </asp:DropDownList>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label runat="server" ID="label1" Text="m" />
                &nbsp;
                    <asp:Label runat="server" ID="label2" Text="222" />
                &nbsp;
             <asp:Button ID="ptarjeta" runat="server" Text="Pagar" OnClick="ptarjeta_Click" />
            </div>
            <div id="efectivo" runat="server">
                <h3>Pago en Efectivo </h3>
                <br />
                <asp:Label runat="server" Text="Ingrese la Cantidad " />
                &nbsp;&nbsp;<asp:TextBox ID="pagoefectivo" runat="server" />
                &nbsp;&nbsp;&nbsp;
                    <asp:DropDownList ID="tiposmonedas" runat="server" AutoPostBack="True" OnSelectedIndexChanged="tiposmonedas_SelectedIndexChanged">
                        <asp:ListItem Text="Seleccione" Value="0" />
                    </asp:DropDownList>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label runat="server" ID="simbolo" Text="m" />
                &nbsp;
                    <asp:Label runat="server" ID="conversion" Text="222" />



                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;



                    <asp:Button ID="pefectivo" runat="server" Text="Pagar" OnClick="pefectivo_Click" />
            </div>

        </div>

    </body>
    </html>



</asp:Content>
