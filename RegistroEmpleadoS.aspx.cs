﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;

namespace Empresa
{
    public partial class RegistroEmpleadoS : System.Web.UI.Page
    {



        protected void Page_Load(object sender, EventArgs e)
        {
            jefes.Items.Add("Seleccione");
            nuevocarro.Visible = false;
            theDiv.Visible = false;
            //if (!this.IsPostBack)
            //{
            //    DataTable dt = new DataTable();
            //    dt.Columns.AddRange(new DataColumn[6] { new DataColumn("Placas", typeof(string)),
            //                new DataColumn("NumeroMotor", typeof(long)),
            //                new DataColumn("NumeroChasis",typeof(long)),
            //                new DataColumn("Marca",typeof(string)),

            //                new DataColumn("Model",typeof(int)),
            //                new DataColumn("Estilo",typeof(string))});
            //    dt.Rows.Add("p-01200", 00000001, 00121515, "Mazda", 1999, "Cerrado");
            //    dt.Rows.Add("p-01040", 00012001, 00151515, "Toyota", 1999, "Extracado");
            //    dt.Rows.Add("p-01100", 08451001, 000515115, "Honda", 1999, "Cerrado");
            //    dt.Rows.Add("p-01850", 014984001, 01111515, "Mazda", 1999, "Cerrado");

            //    GridView1.DataSource = dt;
            //    GridView1.DataBind();
            //}
        }

        SqlConnection sqlCon = new SqlConnection(@"Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;");
        int vendedor = 1;
        int supervisor = 2;
        int gerente = 3;
        int administrador = 4;
        protected void Radio1_CheckedChanged(object sender, EventArgs e)
        {
            jefes.Items.Clear();
            jefes.Items.Add("Seleccione");
            SqlCommand comando = new SqlCommand("select p.Nombre as Puesto, E.Nombre as Jefe from Empleado as E join Puesto as p on p.Codigo = E.Codigo_Puesto where p.Codigo not in (" + vendedor + "," + gerente + "," + administrador + ")", sqlCon);
            sqlCon.Open();
            SqlDataReader Registro = comando.ExecuteReader();
            if (Radio1.Checked == true)
            {
                theDiv.Visible = false;
                while (Registro.Read())
                {
                    jefes.Items.Add(Registro["Jefe"].ToString());

                }
                sqlCon.Close();
            }
            else
            {
                theDiv.Visible = false;
            }


        }
        protected void RadioButton2_CheckedChanged(object sender, EventArgs e)
        {
            jefes.Items.Clear();
            jefes.Items.Add("Seleccione");
            SqlCommand comando = new SqlCommand("select p.Nombre as Puesto, E.Nombre as Jefe from Empleado as E join Puesto as p on p.Codigo = E.Codigo_Puesto where p.Codigo not in (" + vendedor + "," + supervisor + "," + administrador + ")", sqlCon);
            sqlCon.Open();
            SqlDataReader Registro = comando.ExecuteReader();
            if (RadioButton1.Checked == true)
            {
                theDiv.Visible = true;
                while (Registro.Read())
                {
                    jefes.Items.Add(Registro["Jefe"].ToString());

                }
                sqlCon.Close();
                llenadoTabla();

            }
            else
            {
                theDiv.Visible = false;

            }
        }
        protected void RadioButton3_CheckedChanged(object sender, EventArgs e)
        {
            jefes.Items.Clear();
            jefes.Items.Add("Seleccione");
            SqlCommand comando = new SqlCommand("select p.Nombre as Puesto, E.Nombre as Jefe from Empleado as E join Puesto as p on p.Codigo = E.Codigo_Puesto where p.Codigo not in (" + vendedor + "," + gerente + "," + supervisor + ")", sqlCon);
            sqlCon.Open();
            SqlDataReader Registro = comando.ExecuteReader();
            if (RadioButton2.Checked == true)
            {
                theDiv.Visible = true;

                while (Registro.Read())
                {
                    jefes.Items.Add(Registro["Jefe"].ToString());

                }
                sqlCon.Close();
                llenadoTabla();
            }
            else
            {
                theDiv.Visible = false;
            }
        }

        protected void Registro_Click(object sender, EventArgs e)
        {
            int puesto = 0;

            sqlCon.Open();
            if (Radio1.Checked == true)
            {
                puesto = 1;
                SqlDataAdapter sqlDa = new SqlDataAdapter("INSERT INTO Empleado(NIT, Nombre, Apellido, Nacimiento, Direccion,Telefono, Celular, Email, Contraseña, Codigo_Puesto, Codigo_Jefe) values('" + NIT.Text + "','" + Names.Text + "','" +
    LastName.Text + "','" + DateBeborn.Text + "','" + Direccion.Text + "','" + HomePhone.Text + "','" + Celphone.Text + "','" + Correo.Text + "','" + Contraseña.Text + "'," + puesto + ",'" + tipoempleado(jefes.SelectedValue.ToString()).ToString() + "')", sqlCon);

                DataTable dtbl = new DataTable();

                sqlDa.Fill(dtbl);
                sqlCon.Close();
            }
            else if (RadioButton1.Checked == true)
            {
                int placa = Int32.Parse(placaquita.Text);
                puesto = 2;
                SqlDataAdapter sqlDa = new SqlDataAdapter("INSERT INTO Empleado(NIT, Nombre, Apellido, Nacimiento, Direccion,Telefono, Celular, Email, Contraseña, Codigo_Puesto, Codigo_Jefe, PlacaVehiculo) values('" + NIT.Text + "','" + Names.Text + "','" +
    LastName.Text + "','" + DateBeborn.Text + "','" + Direccion.Text + "','" + HomePhone.Text + "','" + Celphone.Text + "','" + Correo.Text + "','" + Contraseña.Text + "'," + puesto + ",'" + tipoempleado(jefes.SelectedValue.ToString()).ToString() + "'," + placa + ")", sqlCon);

                DataTable dtbl = new DataTable();

                sqlDa.Fill(dtbl);
            }
            else if (RadioButton2.Checked == true)
            {
                int placa = Int32.Parse(placaquita.Text);
                puesto = 3;
                SqlDataAdapter sqlDa = new SqlDataAdapter("INSERT INTO Empleado(NIT, Nombre, Apellido, Nacimiento, Direccion,Telefono, Celular, Email, Contraseña, Codigo_Puesto, Codigo_Jefe, PlacaVehiculo) values('" + NIT.Text + "','" + Names.Text + "','" +
    LastName.Text + "','" + DateBeborn.Text + "','" + Direccion.Text + "','" + HomePhone.Text + "','" + Celphone.Text + "','" + Correo.Text + "','" + Contraseña.Text + "'," + puesto + ",'" + tipoempleado(jefes.SelectedValue.ToString()).ToString() + "'," + placa + ")", sqlCon);

                DataTable dtbl = new DataTable();

                sqlDa.Fill(dtbl);
            }

            sqlCon.Close();



            Response.Write("<script>alert('Empleado Registrado')</script>");
        }



        public int tipoempleado(String nombre)
        {
            SqlConnection sqlCon = new SqlConnection(@"Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;");

            int valor = 0;
            sqlCon.Open();
            SqlCommand cmd = new SqlCommand("select p.Nombre as Puesto, E.Nombre as Jefe, E.NIT from Empleado as E join Puesto as p on p.Codigo=E.Codigo_Puesto where E.Nombre='" + nombre + "'", sqlCon);

            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read() == true)
            {

                valor = Int32.Parse(dr["NIT"].ToString());

                sqlCon.Close();
            }

            return valor;
        }

        public void llenadoTabla()
        {
            string connectionString = @"Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                sqlCon.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter("select * from Vehiculo", sqlCon);
                DataTable dtbl = new DataTable();
                sqlDa.Fill(dtbl);
                GridView1.DataSource = dtbl;
                GridView1.DataBind();

            }
        }

        protected void carro_Click(object sender, EventArgs e)
        {
            theDiv.Visible = true;
            nuevocarro.Visible = true;

        }

        protected void crearvehiculo_Click(object sender, EventArgs e)
        {

            int placa = Int32.Parse(Nplaca.Text);
            long motor = long.Parse(Nmotor.Text);
            long chasis = long.Parse(Nchasis.Text);
            int model = Int32.Parse(Modelo.Text);
            double km = double.Parse(Modelo.Text);
            SqlDataAdapter sqlDa = new SqlDataAdapter("INSERT INTO Vehiculo VALUES(" + placa + "," + motor + "," + chasis + ",'" + Marca.Text + "'," + model + ",'" + Estilo.Text + "'," + km + ");", sqlCon);


            DataTable dtbl = new DataTable();

            sqlDa.Fill(dtbl);
            sqlCon.Close();
            llenadoTabla();
            nuevocarro.Visible = false;
            theDiv.Visible = true;
        }




    }
}