﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Security.Policy;
using System.Configuration.Internal;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Xml.Linq;
using Org.BouncyCastle.Bcpg.OpenPgp;
using System.Windows.Forms;

namespace Empresa
{
    public partial class WebForm11 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            tabla();
            llenadomoneda();
            int dato = porden.Text.Length;
            if (dato > 0)
            {
                pago.Visible = true;
                string nombre = pagos.SelectedValue.ToString();
               
                if (nombre.Equals("1"))
                {
                    cheque.Visible = false;
                    efectivo.Visible = true;
                    tarjeta.Visible = false;
                }
                else if (nombre.Equals("2"))
                {
                    cheque.Visible = false;
                    efectivo.Visible = false;
                    tarjeta.Visible = true;
                }
                else if (nombre.Equals("3"))
                {
                    cheque.Visible = true;
                    efectivo.Visible = false;
                    tarjeta.Visible = false;
                }
            }
            else
            {
                cheque.Visible = false;
                efectivo.Visible = false;
                tarjeta.Visible = false;
            }

        }

        public void tabla()
        {
            string connectionString = @"Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {

                sqlCon.Open();
                SqlDataAdapter sqlDa = new SqlDataAdapter("select * from Orden", sqlCon);
                DataTable dtbl = new DataTable();
                sqlDa.Fill(dtbl);
                GridView1.DataSource = dtbl;
                GridView1.DataBind();

            }
        }



        protected void Orden_Click(object sender, EventArgs e)
        {
            string connectionString = @"Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                string dato = "";
                if (Radio1.Checked == true)
                {
                    dato = "Aprobada";

                }
                if (RadioButton1.Checked == true)
                {
                    dato = "No Aporbado";

                }
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "estadorden";
                cmd.Parameters.Add("@nit", SqlDbType.Text).Value = NitJefe.Text;
                cmd.Parameters.Add("@fecha", SqlDbType.DateTime).Value = DateTime.Parse(fechaauto.Text);
                cmd.Parameters.Add("@estado", SqlDbType.Text).Value = dato;
                cmd.Parameters.Add("@orden", SqlDbType.BigInt).Value = (Int32.Parse(CodigoOrden.Text));
                cmd.Connection = sqlCon;
                sqlCon.Open();
                cmd.ExecuteNonQuery();



                Response.Write("<script>alert('actualizada')</script>");

                sqlCon.Close();
                tabla();

            }
        }

        protected void cobrar_Click(object sender, EventArgs e)
        {
            
            if (aprobacion().Equals("Aprobada"))
            {
                pago.Visible = true;
                pagorden();
            }
            else
            {
                Response.Write("<script>alert('Orden no esta Aprobada para pagar')</script>");
            }
        }

        /// Hola que tal, me entere de la noticia de tu papa, pero lo unico 



        public void llenadomoneda()

        {
            SqlConnection conexion = new SqlConnection();
            conexion.ConnectionString = "Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";

            SqlCommand comando = new SqlCommand("select * from Moneda", conexion);
            conexion.Open();
            SqlDataReader Registro = comando.ExecuteReader();
            while (Registro.Read())
            {
                tiposmonedas.Items.Add(Registro["NombreM"].ToString());
                monedas2.Items.Add(Registro["NombreM"].ToString());
                monedas3.Items.Add(Registro["NombreM"].ToString());



            }
            conexion.Close();
        }

        protected void tiposmonedas_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlConnection conexion = new SqlConnection();
            conexion.ConnectionString = "Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";
            conexion.Open();
            SqlCommand cmd = new SqlCommand("SELECT* FROM Moneda where NombreM='" + tiposmonedas.SelectedValue.ToString() + "'", conexion);

            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read() == true)
            {
                double precio = double.Parse(deuda.Text)/double.Parse(dr["Taza"].ToString())  ;
                simbolo.Text = dr["NombreM"].ToString();
                conversion.Text = precio.ToString();
                label3.Text = dr["NombreM"].ToString();
                label4.Text = precio.ToString();
                label1.Text = dr["NombreM"].ToString();
                label2.Text = precio.ToString();
                conexion.Close();
            }
        }

        protected void pcheque_Click(object sender, EventArgs e)
        {
            string connectionString = @"Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";
            double pp = double.Parse(TextBox2.Text);

            if ((pp * tasa(monedas3.SelectedValue.ToString())) <= double.Parse(deuda.Text))
            {
                using (SqlConnection sqlCon = new SqlConnection(connectionString))
                {

                    string pago = "Cheque";
                    sqlCon.Open();
                    int d = Int32.Parse(porden.Text);
                    double pagodolar = pp * tasa(monedas3.SelectedValue.ToString());
                    double pagopendietne = double.Parse(deuda.Text) - pagodolar;
                    SqlDataAdapter sqlDa = new SqlDataAdapter("INSERT INTO pagos VALUES (" + d + ",'" + pago + "'," + pagodolar + ",'" + label3.Text + "','" + tasa(monedas3.SelectedValue.ToString()).ToString() + "','" + DateTime.Parse(fechapago.Text) + "')", sqlCon);
                    DataTable dtbl = new DataTable();

                    sqlDa.Fill(dtbl);
                    sqlCon.Close();
                }

                actualizaciondeuda(pp, monedas3.SelectedValue.ToString());

            }
            else
            {
                Response.Write("<script>alert('Esta pagando mas')</script>");
            }





        }

        protected void ptarjeta_Click(object sender, EventArgs e)
        {
            string connectionString = @"Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";
            double pp = double.Parse(TextBox1.Text);
            
            

            if ((pp * tasa(monedas2.SelectedValue.ToString())) <= double.Parse(deuda.Text))
            {
                using (SqlConnection sqlCon = new SqlConnection(connectionString))
                {

                    string pago = "Tarjeta";
                    sqlCon.Open();
                    int d = Int32.Parse(porden.Text);
                    double pagodolar = pp * tasa(monedas2.SelectedValue.ToString());
                    double pagopendietne = double.Parse(deuda.Text) - pagodolar;
                    SqlDataAdapter sqlDa = new SqlDataAdapter("INSERT INTO pagos VALUES (" + d + ",'" + pago + "'," + pagodolar + ",'" + label1.Text + "',' " + tasa(monedas2.SelectedValue.ToString()) + "','"+DateTime.Parse(fechapago.Text)+"')", sqlCon);
                    DataTable dtbl = new DataTable();

                    sqlDa.Fill(dtbl);
                    sqlCon.Close();
                }
                actualizaciondeuda(pp, monedas2.SelectedValue.ToString());
            }
            else
            {
                Response.Write("<script>alert('Esta pagando mas')</script>");
            }






        }

        protected void pefectivo_Click(object sender, EventArgs e)
        {
            double pp = double.Parse(pagoefectivo.Text);
            

            if ((pp * tasa(tiposmonedas.SelectedValue.ToString())) <= double.Parse(deuda.Text))
            {
                string connectionString = @"Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";

                using (SqlConnection sqlCon = new SqlConnection(connectionString))
                {

                    string pago = "Efectivo";
                    sqlCon.Open();
                    int d = Int32.Parse(porden.Text);
                    double pagodolar = pp * tasa(tiposmonedas.SelectedValue.ToString());
                    double pagopendietne = double.Parse(deuda.Text)-pagodolar;
                    SqlDataAdapter sqlDa = new SqlDataAdapter("INSERT INTO pagos VALUES (" + d + ",'" + pago + "'," + pagodolar + ",'" + simbolo.Text + "','" + tasa(tiposmonedas.SelectedValue.ToString()) + "','" + DateTime.Parse(fechapago.Text) + "')", sqlCon);
                    DataTable dtbl = new DataTable();

                    sqlDa.Fill(dtbl);
                    sqlCon.Close();
                }
                actualizaciondeuda(pp, tiposmonedas.SelectedValue.ToString());
            }
            else
            {
                Response.Write("<script>alert('Esta pagando mas')</script>");
            }
        }

        protected void pagos_SelectedIndexChanged(object sender, EventArgs e)
        {

        }




        public void pagorden()
        {
            SqlConnection conexion = new SqlConnection();
            conexion.ConnectionString = "Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";

            SqlCommand comando = new SqlCommand("select * from Orden where Numero_Orden="+Int32.Parse(porden.Text), conexion);
            conexion.Open();
            SqlDataReader Registro = comando.ExecuteReader();
            while (Registro.Read())
            {
                
                deuda.Text = Registro["Total"].ToString();

            }
            
            conexion.Close();
        }


        public void actualizaciondeuda(double dinero,string combo)
        {
            string connectionString = @"Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "restardeuda";
                cmd.Parameters.Add("@orden", SqlDbType.BigInt).Value = Int32.Parse(porden.Text);
                double pago = dinero * tasa(combo);
                cmd.Parameters.Add("@abono", SqlDbType.Decimal).Value = pago;
                cmd.Connection = sqlCon;
                sqlCon.Open();
                cmd.ExecuteNonQuery();
            }

        }


        public double tasa(string combo)
        {
            double tasa = 0;

            SqlConnection conexion = new SqlConnection();
            conexion.ConnectionString = "Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";
            conexion.Open();
            SqlCommand cmd = new SqlCommand("SELECT* FROM Moneda where NombreM='" + combo + "'", conexion);

            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read() == true)
            {
                tasa = double.Parse(dr["Taza"].ToString());
                

            }
            return tasa;
        }

        public string aprobacion()
        {
            string tasa = "";

            SqlConnection conexion = new SqlConnection();
            conexion.ConnectionString = "Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";
            conexion.Open();
            SqlCommand cmd = new SqlCommand("SELECT* FROM Orden where Numero_Orden=" + Int32.Parse(porden.Text) + "", conexion);

            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read() == true)
            {
                tasa = dr["Aprobacion"].ToString();


            }
            return tasa;
        }

        protected void monedas2_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlConnection conexion = new SqlConnection();
            conexion.ConnectionString = "Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";
            conexion.Open();
            SqlCommand cmd = new SqlCommand("SELECT* FROM Moneda where NombreM='" + monedas2.SelectedValue.ToString() + "'", conexion);

            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read() == true)
            {
                double precio = double.Parse(deuda.Text) / double.Parse(dr["Taza"].ToString());
                simbolo.Text = dr["NombreM"].ToString();
                conversion.Text = precio.ToString();
                label3.Text = dr["NombreM"].ToString();
                label4.Text = precio.ToString();
                label1.Text = dr["NombreM"].ToString();
                label2.Text = precio.ToString();
                conexion.Close();
            }
        }

        protected void monedas3_SelectedIndexChanged(object sender, EventArgs e)
        {
            SqlConnection conexion = new SqlConnection();
            conexion.ConnectionString = "Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";
            conexion.Open();
            SqlCommand cmd = new SqlCommand("SELECT* FROM Moneda where NombreM='" + monedas3.SelectedValue.ToString() + "'", conexion);

            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read() == true)
            {
                double precio = double.Parse(deuda.Text) / double.Parse(dr["Taza"].ToString());
                simbolo.Text = dr["NombreM"].ToString();
                conversion.Text = precio.ToString();
                label3.Text = dr["NombreM"].ToString();
                label4.Text = precio.ToString();
                label1.Text = dr["NombreM"].ToString();
                label2.Text = precio.ToString();
                conexion.Close();
            }
        }


        protected void anulacion_Click(object sender, EventArgs e)
        {

            string connectionString = @"Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "anulacion";
                cmd.Parameters.Add("@orden", SqlDbType.BigInt).Value = Int32.Parse(ordencancelada.Text);
                cmd.Connection = sqlCon;
                sqlCon.Open();
                cmd.ExecuteNonQuery();
            }

            pdf();
        }

        
        public void pdf()
        {
            string codigopago = "";
            string nit = "";
            string nombre = "";
            string apellido = "";
            string direccion = "";
            string nitv = "";
            string nombreempleado = "";
            string puesto = "";
            string telefono = "";
           
            SqlConnection conexion = new SqlConnection();
            conexion.ConnectionString = "Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";
            conexion.Open();
            SqlCommand cmd = new SqlCommand("select NumeroOrden, c.Telefono,c.NIT, c.Nombre, c.Apellido, c.Direccion, Nit_Empleado, E.Nombre+' '+E.Apellido as NombreCompleto, p.Nombre as Puesto  from Cliente as c join Orden on Orden.Nit_Cliente=Nit_Cliente join DetalleOrden on Orden.Nit_Cliente=Nit_Cliente  join Empleado as E on E.NIT=Orden.Nit_Empleado join Puesto as p on p.Codigo=E.Codigo_Puesto  join pagos on Numero_Orden=" + Int32.Parse(ordencancelada.Text) , conexion);
            
            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read() == true)
            {
                 codigopago = dr["NumeroOrden"].ToString();
                 nit = dr["NIT"].ToString();
                nombre = dr["Nombre"].ToString();
                apellido = dr["Apellido"].ToString();
                direccion = dr["Direccion"].ToString();

                nitv = dr["Nit_Empleado"].ToString();
                nombreempleado = dr["NombreCompleto"].ToString();
                puesto = dr["Puesto"].ToString();
                telefono = dr["Telefono"].ToString();







            }
            conexion.Close();
            label5.Text = nombre;
            documento(codigopago, nit, nombre, apellido, direccion, nitv,nombreempleado,puesto);
            factura(codigopago, nit, nombre, apellido, direccion, nombreempleado,telefono);

        }


        public double devolucion()
        {
            double valor =0 ;
            SqlConnection conexion = new SqlConnection();
            conexion.ConnectionString = "Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";
            conexion.Open();
            SqlCommand cmd = new SqlCommand("select idpago, sum(Cantidad) AS Pagado from pagos group by idpago having idpago=" + Int32.Parse(ordencancelada.Text) + "", conexion);

            SqlDataReader dr = cmd.ExecuteReader();
            if (dr.Read() == true)
            {
                valor = double.Parse(dr["Pagado"].ToString());
              

            }
            conexion.Close();


            return valor;



        }

        public void documento(string codigopago, string nit, string nombre, string apellido, string direccion,string nitv,string nombrecompleto, string puesto)
        {
            DataTable dt = new DataTable();
            FileStream fs = new FileStream("C:/Users/myale/Downloads/"+archivonombre.Text+ ".pdf", FileMode.Create);
            Document document = new Document(iTextSharp.text.PageSize.LETTER, 25, 40, 25, 40);
            PdfWriter pw = PdfWriter.GetInstance(document, fs);
            document.Open();
            
            document.Add(new Phrase("\t\t\t\tRECIBO DE PAGO \n\n\n\n" ));
            Phrase pa = new Phrase();

            document.Add(new Chunk("Codigo de la orden  : " + codigopago.ToString()));
            document.Add(new Chunk("\nNit : " + nit.ToString() + " Nombre : " + nombre.ToString() + " " + apellido.ToString() + "  Direccion : " + direccion.ToString()));
            document.Add(new Phrase("\nNit de Vendedor : "+ nitv +"  Nombre Empleado : "+ nombrecompleto+ "  Puesto : "+puesto));


            dt = detallepago();
            if (dt.Rows.Count > 0)
            {
                PdfPTable table = new PdfPTable(dt.Columns.Count);
                document.Add(new Chunk("\n\n\nDetalle de Pago Realizados"));
                document.Add(new Chunk("\n"));

                float[] widths = new float[dt.Columns.Count];
                for (int i = 0; i < dt.Columns.Count; i++) widths[i] = 4f;

                table.SetWidths(widths);
                table.WidthPercentage = 90;

                PdfPCell cell = new PdfPCell(new Phrase("columns"));
                cell.Colspan = dt.Columns.Count;

                foreach (DataColumn c in dt.Columns)
                {
                    table.AddCell(new Phrase(c.ColumnName));

                }

                foreach (DataRow r in dt.Rows)
                {
                    for (int h = 0; h < dt.Columns.Count; h++)
                    {
                        table.AddCell(new Phrase(r[h].ToString()));
                    }

                }

                document.Add(table);
            }

            document.Add(new Chunk("\n\nValor de este pago : $. " + devolucion()));
            document.Add(new Chunk("\nSaldo Pendiente : $. 0 "));
            document.Add(new Chunk("\nDetalle del Pago: \n\n\n  \t\t\t\t\t\t\t\tANULACION DE VENTA\n\n\n  \t\t\t\t\t\t\t\tDEVOLUCION DE DINERO"));


            document.Close();
      
        }





        public DataTable detallepago()
        {
            DataTable dt = new DataTable();
            string connectionString = @"Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "detallepago";
                cmd.Parameters.Add("@orden", SqlDbType.BigInt).Value = Int32.Parse(ordencancelada.Text);
                cmd.Connection = sqlCon;
                sqlCon.Open();

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                da.Dispose();

            }

            return dt;
        }


        public void detalleanulacion()
        {
            string connectionString = @"Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "notaanulacion";
                cmd.Parameters.Add("@orden", SqlDbType.BigInt).Value = Int32.Parse(ordencancelada.Text);
                cmd.Connection = sqlCon;
                sqlCon.Open();
                cmd.ExecuteNonQuery();
            }
        }


        public void factura(string codigopago, string nit, string nombre, string apellido, string direccion, string nombrecompleto,string telefono)
        {
            DataTable dt = new DataTable();
            FileStream fs = new FileStream("C:/Users/myale/Downloads/factura_" + archivonombre.Text + ".pdf", FileMode.Create);
            Document document = new Document(iTextSharp.text.PageSize.LETTER, 25, 40, 25, 40);
            PdfWriter pw = PdfWriter.GetInstance(document, fs);
            document.Open();

            document.Add(new Phrase("\t\t\t\t\t\t\t\t\tFactura \n\n\n\n"));
            Phrase pa = new Phrase();

            document.Add(new Chunk("Codigo de la orden  : " + codigopago.ToString()));
            document.Add(new Chunk("\nNit : 101010  Nombre : Diprona S.A  Direccion : Guatemala, Capital Zona 8  Telefono : 55555552" ));

            document.Add(new Chunk("\nNit : " + nit.ToString() + " Nombre : " + nombre.ToString() + " " + apellido.ToString() + "  Direccion : " + direccion.ToString() + " Telefono : "+ telefono.ToString()));
            document.Add(new Chunk("\nMonto Total de Factura  : $ " + devolucion()));

            document.Add(new Chunk("\nFecha de Facurizacion  : " + DateTime.Parse(fechapago.Text).ToString()));
            document.Add(new Chunk("\nNombre de Vendedor  : " + nombrecompleto.ToString()));

            dt = detallefactura();
            if (dt.Rows.Count > 0)
            {
                PdfPTable table = new PdfPTable(dt.Columns.Count);
                document.Add(new Chunk("\n\n\nDetalle De Articulos adquiridos"));
                document.Add(new Chunk("\n"));

                float[] widths = new float[dt.Columns.Count];
                for (int i = 0; i < dt.Columns.Count; i++) widths[i] = 4f;

                table.SetWidths(widths);
                table.WidthPercentage = 90;

                PdfPCell cell = new PdfPCell(new Phrase("columns"));
                cell.Colspan = dt.Columns.Count;

                foreach (DataColumn c in dt.Columns)
                {
                    table.AddCell(new Phrase(c.ColumnName));

                }

                foreach (DataRow r in dt.Rows)
                {
                    for (int h = 0; h < dt.Columns.Count; h++)
                    {
                        table.AddCell(new Phrase(r[h].ToString()));
                    }

                }

                document.Add(table);
            }

            
            document.Add(new Chunk("\nSaldo Pendiente : $. 0 "));
            document.Add(new Chunk("\nDetalle del Pago: \n\n\n  \t\t\t\t\t\t\t\tANULACION DE VENTA\n\n\n  \t\t\t\t\t\t\t\tDEVOLUCION DE DINERO"));


            document.Close();
        }


        public DataTable detallefactura()
        {
            DataTable dt = new DataTable();
            string connectionString = @"Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;";

            using (SqlConnection sqlCon = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "detallefactura";
                cmd.Parameters.Add("@orden", SqlDbType.BigInt).Value = Int32.Parse(ordencancelada.Text);
                cmd.Connection = sqlCon;
                sqlCon.Open();

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(dt);
                da.Dispose();

            }

            return dt;
        }


    }
}
