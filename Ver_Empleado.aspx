﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Administrador.Master" AutoEventWireup="true" CodeBehind="Ver_Empleado.aspx.cs" Inherits="Empresa.WebForm7" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <!DOCTYPE html>

    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Empleados</title>
        <style type="text/css">
            .header {
                height: 16px;
                width: 25px;
                background-color:bisque;
            }
            .scrolling-table-container {
                height: 378px;
                overflow-y: scroll;
                overflow-x: hidden;
            }
        </style>
    </head>
    <body>
        <h3 style="text-align:center">Empleados</h3>
        <b></b>
        <br />
        <div class="col-m-6">
            <div class="scrolling-table-container">

                <asp:GridView ID="GridView1" HeaderStyle-BackColor="#3AC0F2" HeaderStyle-ForeColor="White"
                    runat="server" AutoGenerateColumns="False" align="center">
                    <Columns>
                        <asp:BoundField DataField="NIT" HeaderText="NIT" ItemStyle-BackColor="Yellow">
                            <ItemStyle BackColor="Yellow"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="Nombre" HeaderText="Nombre" ItemStyle-BackColor="Yellow">
                            <ItemStyle BackColor="Yellow"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="Apellido" HeaderText="Apellido" ItemStyle-BackColor="Yellow">
                            <ItemStyle BackColor="Yellow"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="Direccion" HeaderText="Direccion" ItemStyle-BackColor="Yellow">
                            <ItemStyle BackColor="Yellow"></ItemStyle>
                        </asp:BoundField>

                        <asp:BoundField DataField="Telefono" HeaderText="Telefono" ItemStyle-BackColor="Yellow">
                            <ItemStyle BackColor="Yellow"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="Celular" HeaderText="Celular" ItemStyle-BackColor="Yellow">
                            <ItemStyle BackColor="Yellow"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="Celular" HeaderText="Celular" ItemStyle-BackColor="Yellow">
                            <ItemStyle BackColor="Yellow"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="Contraseña" HeaderText="Contraseña" ItemStyle-BackColor="Yellow">
                            <ItemStyle BackColor="Yellow"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="Puesto" HeaderText="Puesto" ItemStyle-BackColor="Yellow">
                            <ItemStyle BackColor="Yellow"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="Jefe" HeaderText="Jefe" ItemStyle-BackColor="Yellow">
                            <ItemStyle BackColor="Yellow"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="Marca_del_Vehiculo" HeaderText="Marca_del_Vehiculo" ItemStyle-BackColor="Yellow">
                            <ItemStyle BackColor="Yellow"></ItemStyle>
                        </asp:BoundField>


                    </Columns>

                    <HeaderStyle BackColor="#3AC0F2" ForeColor="White"></HeaderStyle>
                </asp:GridView>
            </div>
        </div>
        <div class="col-m-6">
            <div>
                <b></b>
                <br />
                <asp:Label ID="LA" runat="server" Text="Ingrese el Nit " />
                <br />
                <br />
                <asp:TextBox ID="CODIGO" runat="server" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="Eliminar" runat="server" Text="Eliminar Empleado" OnClick="Eliminar_Click" />
                &nbsp;
                <asp:Button ID="Editar" runat="server" Text="Editar Empleado" OnClick="Editar_Click" />

                <br />

            </div>


        </div>
        <br />
        <br />
        <div id="Edicion" runat="server">
            <div class="row align-items-center">
                <div class="col-md-6  ">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="form-group">
                                <label>
                                    Correo                       
                                    <label>Correo</label>
                            </div>
                            <div class="form-group">
                                <asp:TextBox ID="Correo" runat="server" CssClass="form-control" placeholder="ejemlo@gmail.com"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label>Contraseña</label>
                            </div>
                            <div class="form-group">
                                <asp:TextBox ID="Contraseña" runat="server" CssClass="form-control" placeholder="*********"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label>Telefono</label>
                            </div>
                            <div class="form-group">
                                <asp:TextBox ID="Celphone" runat="server" CssClass="form-control" placeholder="12045782" required="required"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-md-6 ">
                    <div class="box box-primary">
                        <div class="box-body">
                            <div class="form-group">
                                <label>Domicilio</label>
                            </div>
                            <div class="form-group">
                                <asp:TextBox ID="Direccion" runat="server" CssClass="form-control" placeholder="Guatemala, Ciudad Capital , Zona 8 5-5" required="required"></asp:TextBox>
                            </div>

                            <div class="form-group">
                                <label>Telefono de casa</label>
                            </div>
                            <div class="form-group">
                                <asp:TextBox ID="HomePhone" runat="server" CssClass="form-control" placeholder="21457852"></asp:TextBox>
                            </div>


                            <br />

                            <br />
                            <asp:Button ID="Actualizar" runat="server" Text="Actualizar" OnClick="Actualizar_Click" />

                        </div>
                    </div>
                </div>

            </div>



        </div>

    </body>
    </html>
</asp:Content>
