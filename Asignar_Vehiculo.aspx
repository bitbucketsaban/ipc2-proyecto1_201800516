﻿<%@ Page Title="Asignar Vehiculo" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Asignar_Vehiculo.aspx.cs" Inherits="Empresa.WebForm6" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">



    <!DOCTYPE html>

    <html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Empleados</title>
        <style type="text/css">
            .header {
                height: 16px;
                width: 25px;
            }
        </style>
    </head>
    <body>

        <h3 style="text-align: center">Asignando Vehiculos a Empleados</h3>

        <div>
            <b></b>
            <br />
            <h5 style="text-align: center; color:red" >Vehiculos Disponibles</h5>

            <asp:GridView ID="GridView1" HeaderStyle-BackColor="#3AC0F2" HeaderStyle-ForeColor="White"
                runat="server" AutoGenerateColumns="False" align="center">
                <Columns>
                    <asp:BoundField DataField="Placas" HeaderText="Placas" ItemStyle-BackColor="Yellow">
                        <ItemStyle BackColor="Yellow"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="NumeroMotor" HeaderText="Numero De Motor" ItemStyle-BackColor="Yellow">
                        <ItemStyle BackColor="Yellow"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="NumeroChasis" HeaderText="Numero De Chasis" ItemStyle-BackColor="Yellow">
                        <ItemStyle BackColor="Yellow"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="Marca" HeaderText="Marca" ItemStyle-BackColor="Yellow">
                        <ItemStyle BackColor="Yellow"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="Model" HeaderText="Modelo" ItemStyle-BackColor="Yellow">
                        <ItemStyle BackColor="Yellow"></ItemStyle>
                    </asp:BoundField>
                    <asp:BoundField DataField="Estilo" HeaderText="Estilo" ItemStyle-BackColor="Yellow">

                        <ItemStyle BackColor="Yellow"></ItemStyle>
                    </asp:BoundField>
                    <asp:CommandField ShowSelectButton="True" />

                </Columns>

                <HeaderStyle BackColor="#3AC0F2" ForeColor="White"></HeaderStyle>
            </asp:GridView>
        </div>

    </body>
    </html>


</asp:Content>
