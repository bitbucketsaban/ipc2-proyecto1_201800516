﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Supervisor.Master" AutoEventWireup="true" CodeBehind="Crear_OrdenS.aspx.cs" Inherits="Empresa.Crear_OrdenS" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    
    <section class="container-header">
        <h3 style="text-align: center; font: bold 25 px">Crear Nueva Orden</h3>
        <br />
        <br />

    </section>

    <div id="buscadornit" runat="server">
        <asp:Label runat="server" Text="Ingrese NIT" Font-Bold="true" Font-Size="Medium" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:TextBox runat="server" ID="TextBox1" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button runat="server" Text="Buscar" ID="buscaNit" OnClick="buscaNit_Click" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Label runat="server" Text="Orden" Font-Bold="true" Font-Size="Medium" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:TextBox runat="server" ID="IdOrdene" />
        <br />
        <br />

        Nombre:
        
        <asp:Label ID="nom" runat="server" />
        &nbsp;&nbsp;&nbsp; Apellido:&nbsp;
        <asp:Label ID="apell" runat="server" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Telefono :
        <asp:Label ID="telef" runat="server" />


    </div>

    <div class="row align-items-center" id="registro" runat="server">

        <div class="col-md-6  " id="datos" runat="server">
            <div class="box box-primary">
                <div class="box-body">

                    <div class="form-group">
                        <label>Nombre</label>
                    </div>
                    <div class="form-group">
                        <asp:TextBox ID="Names" runat="server" CssClass="form-control" placeholder="Jorge Josue" required="required"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Apellido</label>
                    </div>
                    <div class="form-group">
                        <asp:TextBox ID="LastName" runat="server" CssClass="form-control" placeholder="Perez Gomez" required="required"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Fecha de Nacimiento</label>
                    </div>
                    <div class="form-group">
                        <asp:TextBox ID="DateBeborn" runat="server" CssClass="form-control" placeholder="01/06/1995" required="required"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>NIT</label>
                    </div>
                    <div class="form-group">
                        <asp:TextBox ID="NIT" runat="server" CssClass="form-control" placeholder="123455-6"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Correo Electronico</label>
                    </div>
                    <div class="form-group">
                        <asp:TextBox ID="Correo" runat="server" CssClass="form-control" type="email" required="required" placeholder="juanPerez10Mar@gmail.com"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Limite de Credito</label>
                    </div>
                    <div class="form-group">
                        <asp:TextBox ID="Limite" runat="server" CssClass="form-control" placeholder="Guatemala" required="required"></asp:TextBox>
                    </div>


                </div>
            </div>
        </div>

        <div class="col-md-6 " id="datos2" runat="server">
            <div class="box box-primary">
                <div class="box-body">
                    <div class="form-group">
                        <label>Domicilio</label>
                    </div>
                    <div class="form-group">
                        <asp:TextBox ID="Direccion" runat="server" CssClass="form-control" placeholder="Guatemala, Ciudad Capital , Zona 8 5-5" required="required"></asp:TextBox>
                    </div>

                    <div class="form-group">
                        <label>Telefono de casa</label>
                    </div>
                    <div class="form-group">
                        <asp:TextBox ID="HomePhone" runat="server" CssClass="form-control" placeholder="21457852"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Telefono</label>
                    </div>
                    <div class="form-group">
                        <asp:TextBox ID="Celphone" runat="server" CssClass="form-control" placeholder="12045782" required="required"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Departamento</label>
                    </div>
                    <div class="form-group">
                        <asp:TextBox ID="Departament" runat="server" CssClass="form-control" placeholder="Guatemala"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Municipio</label>
                    </div>
                    <div class="form-group">
                        <asp:TextBox ID="Municipio" runat="server" CssClass="form-control" placeholder="Guatemala" required="required"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Dias de Credito</label>
                    </div>
                    <div class="form-group">
                        <asp:TextBox ID="credito" runat="server" CssClass="form-control" placeholder="Guatemala" required="required"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label>Numero de Orden</label>
                    </div>
                    <div class="form-group">
                        <asp:TextBox ID="IdOrden" runat="server" CssClass="form-control" placeholder="100" required="required"></asp:TextBox>
                    </div>


                </div>
            </div>
            <asp:Button ID="registrar" runat="server" Text="Registrar Cliente" OnClick="registrar_Click" />
        </div>



    </div>

    <div>

        <h3 style="text-align: center">Productos </h3>
        <br />
        <p>Buscar por Categoria</p>
        <div class="form-group">
            <asp:DropDownList ID="Categoria" runat="server" OnSelectedIndexChanged="Categoria_SelectedIndexChanged" AutoPostBack="True">

                <asp:ListItem Text="Equipo de Computo" Value="0" />
                <asp:ListItem Text="Audio" Value="1" />
                <asp:ListItem Text="Video" Value="2" />
                <asp:ListItem Text="Linea Blanca" Value="3" />
                <asp:ListItem Text="Muebles" Value="4" />
                <asp:ListItem Text="VideoJuegos" Value="5" />
                <asp:ListItem Text="Smartphones" Value="6" />
                <asp:ListItem Text="Electrodomesticos" Value="7" />
                <asp:ListItem Text="Electronicos" Value="8" />



            </asp:DropDownList>
            <asp:Label ID="label" runat="server"></asp:Label>

        </div>
        <style>
                    .scrolling-table-container {
                        position:relative;
                        height:200px;
                        overflow:auto;
                    }
                </style>

    </div>

    <br />
    <div class="align-items-end align-container-end">
        <asp:Label Text="Codigo del Producto" runat="server" />
        <asp:TextBox ID="Producto" runat="server" />
        <asp:Button ID="Agregar" runat="server" Text="Agregar a Orden" OnClick="Agregar_Click" />
        <asp:Button ID="editar" runat="server" Text="Editar Orden" OnClick="editar_Click" />

        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Label Text="Carro de Compras" runat="server" Style="align-content: flex-end; align-items: end; color: red; font-size: 25px" />
        <br />
        <div id="editor" runat="server">
            <br />
            <asp:Label Text="Cantidad de Producto" runat="server" />
            <asp:TextBox ID="Cantidad" runat="server" />
            <asp:Button ID="actualizacion" runat="server" Text="Actualizar orden Orden" OnClick="actualizacion_Click" />
            <asp:Button ID="Eliminarp" runat="server" Text="Eliminar Producto" OnClick="eliminar" />
        </div>

    </div>
    <br />
    <br />
    <div>
        <div class="col-md-6 align-items-center ">
            <div class="scrolling-table-container">

                <asp:GridView ID="GridView1" HeaderStyle-BackColor="#3AC0F2" HeaderStyle-ForeColor="White"
                    runat="server" AutoGenerateColumns="False" align="center" OnPageIndexChanging="GridView1_PageIndexChanging"
                    OnSelectedIndexChanged="prueba">

                    <Columns>
                        <asp:BoundField DataField="Codigo_Producto" HeaderText="Codigo" ItemStyle-BackColor="Yellow">
                            <ItemStyle BackColor="Yellow"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="Nombre" HeaderText="Nombre" ItemStyle-BackColor="Yellow">
                            <ItemStyle BackColor="Yellow"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="Precio" HeaderText="Precio" ItemStyle-BackColor="Yellow">
                            <ItemStyle BackColor="Yellow"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="Descripcion" HeaderText="Descripcion" ItemStyle-BackColor="Yellow">
                            <ItemStyle BackColor="Yellow"></ItemStyle>
                        </asp:BoundField>
                        <%-- <asp:BoundField DataField="Marca" HeaderText="Marca" ItemStyle-BackColor="Yellow">

            <ItemStyle BackColor="Yellow"></ItemStyle>
        </asp:BoundField>

        <asp:BoundField DataField="Precio" HeaderText="Precio" ItemStyle-BackColor="Yellow">
            <ItemStyle BackColor="Yellow"></ItemStyle>
        </asp:BoundField>
        <asp:BoundField DataField="Imagen" HeaderText="Imagen" ItemStyle-BackColor="Yellow">
            <ItemStyle BackColor="Yellow"></ItemStyle>
        </asp:BoundField>
        <asp:BoundField DataField="Comentario" HeaderText="Comentario" ItemStyle-BackColor="Yellow">


            <ItemStyle BackColor="Yellow"></ItemStyle>
                        </asp:BoundField>--%>
                    </Columns>


                    <HeaderStyle BackColor="Black" ForeColor="White" Font-Bold="True"></HeaderStyle>
                    <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" />
                    <RowStyle BackColor="White" />
                    <SelectedRowStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="Gray" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#383838" />
                </asp:GridView>
            </div>
        </div>

        <div class="col-md-6 align-items-center">

            <div class="scrolling-table-container">
                <asp:GridView ID="GridView2" HeaderStyle-BackColor="#3AC0F2" HeaderStyle-ForeColor="White"
                    runat="server" AutoGenerateColumns="False" align="center" BackColor="#CCCCCC" BorderColor="#999999" BorderStyle="Solid" BorderWidth="3px" CellPadding="4" CellSpacing="2" ForeColor="Black" RowHeaderColumn="Codigo" OnSelectedIndexChanged="GridView2_SelectedIndexChanged">

                    <Columns>
                        <asp:BoundField DataField="Nombre" HeaderText="Nombre" ItemStyle-BackColor="Yellow">
                            <ItemStyle BackColor="Yellow"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="Cantidad" HeaderText="Cantidad" ItemStyle-BackColor="Yellow">
                            <ItemStyle BackColor="Yellow"></ItemStyle>
                        </asp:BoundField>
                        <asp:BoundField DataField="Total" HeaderText="Total" ItemStyle-BackColor="Yellow">
                            <ItemStyle BackColor="Yellow"></ItemStyle>
                        </asp:BoundField>
                        <%-- <asp:BoundField DataField="Marca" HeaderText="Marca" ItemStyle-BackColor="Yellow">

                            <ItemStyle BackColor="Yellow"></ItemStyle>
                        </asp:BoundField>

                        <asp:BoundField DataField="Precio" HeaderText="Precio" ItemStyle-BackColor="Yellow">
                            <ItemStyle BackColor="Yellow"></ItemStyle>
                        </asp:BoundField>--%>
                    </Columns>

                </asp:GridView>
            </div>

            <asp:Label ID="nombre" runat="server" Text="Total a Pagar : $ " />
            &nbsp;<asp:Label ID="total" runat="server" />

        </div>






    </div>
    <br />
    <br />

    <h3 style="color: salmon; text-align: center">Datos del empleado vendedor</h3>
    <p style="color: salmon; text-align: center">&nbsp;</p>

    <br />
    <br />

    <div class="row align-items-center">

        <div class="col-md-4  " id="Div1" runat="server">
            <div class="box box-primary">
                <div class="box-body">

                    <div class="form-group">
                        <label>NIT del EMPLEADO VENDEDOR </label>
                    </div>
                    <div class="form-group">

                        <asp:TextBox ID="nitempleado" runat="server" CssClass="form-control" placeholder="123455-6" />
                    </div>
                    <div class="form-group">
                        <label>Fecha de creacion de la orden </label>
                    </div>
                    <div class="form-group">

                        <asp:TextBox ID="fechaorden1" runat="server" CssClass="form-control" placeholder="123455-6" />
                    </div>
                    <div class="form-group">
                        <asp:RadioButton ID="Radio1" Text=".  Aprobado" Checked="false" GroupName="RadioGroup1" runat="server" AutoPostBack="false" />
                        <br />
                        <asp:RadioButton ID="RadioButton1" Text=".  No Aprobado" Checked="False" GroupName="RadioGroup1" runat="server" AutoPostBack="false" /><br />

                    </div>

                </div>
            </div>
        </div>

        <div class="col-md-4  " id="Div2" runat="server">
            <div class="box box-primary">
                <div class="box-body">

                    <div class="form-group">
                        <label>NIT del Empleado Aprobador de Orden </label>
                    </div>
                    <div class="form-group">

                        <asp:TextBox ID="nitaprobacion" runat="server" CssClass="form-control" placeholder="123455-6" />
                    </div>
                    <div class="form-group">
                        <label>Fecha de Aprobacion de la orden </label>
                    </div>
                    <div class="form-group">

                        <asp:TextBox ID="aprobacion" runat="server" CssClass="form-control" placeholder="123455-6" />
                    </div>
                    <div class="form-group">
                        <asp:Button ID="CreateProduct" runat="server" Text="Generar Pedido" Height="51px" Width="185px" OnClick="CreateProduct_Click" />
                    </div>

                </div>
            </div>
        </div>





    </div>



</asp:Content>
