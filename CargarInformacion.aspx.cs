﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Xml;
using System.Data;
using System.Data.SqlClient;

namespace Empresa
{
	public partial class WebForm1 : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{

		}

		SqlConnection sqlCon = new SqlConnection(@"Data Source=LAPTOP-E5AQCN7J; Initial Catalog = Diprona; Integrated Security=True;");

		protected void cargar_Click(object sender, EventArgs e)
		{

			categoria();
			empleado();
			producto();
			cliente();
			ciudad();
			departamento();
			Moneda();
			lista();
			listacliente();
		}
		public void categoria()
		{
			XmlDocument doc = new XmlDocument();

			doc.Load(archivo.Text);
			XmlNodeList listaempleado = doc.SelectNodes("definicion/categoria");
			XmlNode unEmpleado;
			///empleados
			for (int i = 0; i < listaempleado.Count; i++)
			{
				unEmpleado = listaempleado.Item(i);

				string codigo = unEmpleado.SelectSingleNode("codigo").InnerText;
				string nombre = unEmpleado.SelectSingleNode("nombre").InnerText;

				int puesto = Int32.Parse(codigo);

				SqlDataAdapter sqlDa = new SqlDataAdapter("INSERT INTO Categoria( Codigo,Nombre) values(" + codigo + ",'" + nombre + "')", sqlCon);

				DataTable dtbl = new DataTable();

				sqlDa.Fill(dtbl);
				sqlCon.Close();

			}
		}

		public void empleado()
		{
			XmlDocument doc = new XmlDocument();

			doc.Load(archivo.Text);
			XmlNodeList listaempleado = doc.SelectNodes("definicion/empleado");
			XmlNode unEmpleado;
			///empleados
			for (int i = 0; i < listaempleado.Count; i++)
			{
				unEmpleado = listaempleado.Item(i);

				string nit = unEmpleado.SelectSingleNode("NIT").InnerText;
				string nombre = unEmpleado.SelectSingleNode("nombres").InnerText;
				string apellidos = unEmpleado.SelectSingleNode("apellidos").InnerText;
				string nacimiento = unEmpleado.SelectSingleNode("nacimiento").InnerText;
				string direccion = unEmpleado.SelectSingleNode("direccion").InnerText;
				string telefono = unEmpleado.SelectSingleNode("telefono").InnerText;
				string celular = unEmpleado.SelectSingleNode("celular").InnerText;
				string email = unEmpleado.SelectSingleNode("email").InnerText;
				string codigoPuesto = unEmpleado.SelectSingleNode("codigo_puesto").InnerText;
				string codigoJefe = unEmpleado.SelectSingleNode("codigo_jefe").InnerText;
				string contra = unEmpleado.SelectSingleNode("pass").InnerText;

				int puesto = Int32.Parse(codigoPuesto);
				DateTime d = DateTime.Parse(nacimiento);
				label1.Text = d.ToString();
				sqlCon.Open();
				if (codigoJefe.Length > 0)
				{
					SqlDataAdapter sqlDa = new SqlDataAdapter("INSERT INTO Empleado(NIT, Nombre, Apellido, Nacimiento, Direccion,Telefono, Celular, Email, Contraseña, Codigo_Puesto, Codigo_Jefe) values('" + nit + "','" + nombre + "','" +
				apellidos + "','" + d + "','" + direccion + "','" + telefono + "','" + celular + "','" + email + "','" + contra + "'," + codigoPuesto + ",'" + codigoJefe + "')", sqlCon);

					DataTable dtbl = new DataTable();

					sqlDa.Fill(dtbl);
					sqlCon.Close();

				}
				else
				{
					SqlDataAdapter sqlDa = new SqlDataAdapter("INSERT INTO Empleado(NIT, Nombre, Apellido, Nacimiento, Direccion,Telefono, Celular, Email, Contraseña, Codigo_Puesto) values('" + nit + "','" + nombre + "','" +
				apellidos + "','" + d + "','" + direccion + "','" + telefono + "','" + celular + "','" + email + "','" + contra + "'," + codigoPuesto + ")", sqlCon);

					DataTable dtbl = new DataTable();

					sqlDa.Fill(dtbl);
					sqlCon.Close();
				}

			}


		}


		public void Moneda()
		{
			XmlDocument doc = new XmlDocument();

			doc.Load(archivo.Text);
			XmlNodeList listaempleado = doc.SelectNodes("definicion/moneda");
			XmlNode unEmpleado;
			///empleados
			for (int i = 0; i < listaempleado.Count; i++)
			{
				unEmpleado = listaempleado.Item(i);

				string nombre = unEmpleado.SelectSingleNode("nombre").InnerText;
				string simb = unEmpleado.SelectSingleNode("simbolo").InnerText;
				string cambio = unEmpleado.SelectSingleNode("tasa").InnerText;

				double dato = double.Parse(cambio); 

				SqlDataAdapter sqlDa = new SqlDataAdapter("INSERT INTO Moneda VALUES ('"+nombre+"','"+simb+"',"+dato+")", sqlCon);

				DataTable dtbl = new DataTable();

				sqlDa.Fill(dtbl);
				sqlCon.Close();

			}
		}


		public void cliente()
		{
			XmlDocument doc = new XmlDocument();

			doc.Load(archivo.Text);
			XmlNodeList listaempleado = doc.SelectNodes("definicion/cliente");
			XmlNode unEmpleado;
			///empleados
			for (int i = 0; i < listaempleado.Count; i++)
			{
				unEmpleado = listaempleado.Item(i);

				string nit = unEmpleado.SelectSingleNode("NIT").InnerText;
				string nombre = unEmpleado.SelectSingleNode("nombres").InnerText;
				string apellidos = unEmpleado.SelectSingleNode("apellidos").InnerText;
				string nacimiento = unEmpleado.SelectSingleNode("nacimiento").InnerText;
				string direccion = unEmpleado.SelectSingleNode("direccion").InnerText;
				string telefono = unEmpleado.SelectSingleNode("telefono").InnerText;
				string celular = unEmpleado.SelectSingleNode("celular").InnerText;
				string email = unEmpleado.SelectSingleNode("email").InnerText;
				string ciudad = unEmpleado.SelectSingleNode("ciudad").InnerText;
				string departamento = unEmpleado.SelectSingleNode("depto").InnerText;
				string limite = unEmpleado.SelectSingleNode("limite_credito").InnerText;
				string dias = unEmpleado.SelectSingleNode("dias_credito").InnerText;
				string codigolista = unEmpleado.SelectSingleNode("codigo_lista").InnerText;

				int dia = Int32.Parse(dias);
				int codigol = Int32.Parse(codigolista);

				double credito = double.Parse(limite);
				DateTime d = DateTime.Parse(nacimiento);
				label1.Text = d.ToString();
				sqlCon.Open();
				SqlDataAdapter sqlDa = new SqlDataAdapter("INSERT INTO Cliente values('" + nit + "', '" + nombre + "', '" + apellidos + "', '" + d + "', '" + direccion + "', '" + telefono + "', '" + celular + "', '" + email + "', 1, " + credito + "," + dia + ", " + codigol + ");", sqlCon);

				DataTable dtbl = new DataTable();

				sqlDa.Fill(dtbl);
				sqlCon.Close();

			}

		}

		public void producto()
		{
			XmlDocument doc = new XmlDocument();

			doc.Load(archivo.Text);
			XmlNodeList listaempleado = doc.SelectNodes("definicion/producto");
			XmlNode unEmpleado;
			///empleados
			for (int i = 0; i < listaempleado.Count; i++)
			{
				unEmpleado = listaempleado.Item(i);

				string codigo = unEmpleado.SelectSingleNode("codigo").InnerText;
				string nombre = unEmpleado.SelectSingleNode("nombre").InnerText;

				string descripcion = unEmpleado.SelectSingleNode("descripcion").InnerText;
				string categoria = unEmpleado.SelectSingleNode("categoria").InnerText;

				int idp = Int32.Parse(codigo);
				int idc = Int32.Parse(categoria);


				SqlDataAdapter sqlDa = new SqlDataAdapter("INSERT INTO Producto(Codigo_Producto,Nombre,IdCategoria,Descripcion) VALUES (" + idp + ",'" + nombre + "'," + idc + ",'" + descripcion + "');", sqlCon);

				DataTable dtbl = new DataTable();

				sqlDa.Fill(dtbl);
				sqlCon.Close();

			}
		}

		public void ciudad()
		{
			XmlDocument doc = new XmlDocument();

			doc.Load(archivo.Text);
			XmlNodeList listaempleado = doc.SelectNodes("definicion/ciudad");
			XmlNode unEmpleado;
			///empleados
			for (int i = 0; i < listaempleado.Count; i++)
			{
				unEmpleado = listaempleado.Item(i);

				string codigo = unEmpleado.SelectSingleNode("codigo").InnerText;
				string iddepartamento = unEmpleado.SelectSingleNode("codigo_departamento").InnerText;
				string nombre = unEmpleado.SelectSingleNode("nombre").InnerText;


				int idciu = Int32.Parse(codigo);
				int iddepa = Int32.Parse(iddepartamento);


				SqlDataAdapter sqlDa = new SqlDataAdapter("INSERT INTO Ciudad values (" + idciu + "," + iddepa + ",'" + nombre + "');", sqlCon);

				DataTable dtbl = new DataTable();

				sqlDa.Fill(dtbl);
				sqlCon.Close();

			}

		}

		public void departamento()
		{
			XmlDocument doc = new XmlDocument();

			doc.Load(archivo.Text);
			XmlNodeList listaempleado = doc.SelectNodes("definicion/depto");
			XmlNode unEmpleado;
			///empleados
			for (int i = 0; i < listaempleado.Count; i++)
			{
				unEmpleado = listaempleado.Item(i);

				string codigo = unEmpleado.SelectSingleNode("codigo").InnerText;
				string nombre = unEmpleado.SelectSingleNode("nombre").InnerText;

				int idc = Int32.Parse(codigo);

				SqlDataAdapter sqlDa = new SqlDataAdapter("INSERT INTO Departamento values (" + idc + ",'" + nombre + "');", sqlCon);

				DataTable dtbl = new DataTable();

				sqlDa.Fill(dtbl);
				sqlCon.Close();

			}
		}


		public void lista()
		{
			XmlDocument doc = new XmlDocument();

			doc.Load(archivo.Text);
			XmlNodeList listaempleado = doc.SelectNodes("definicion/lista");
			XmlNode unEmpleado;
			///empleados
			for (int i = 0; i < listaempleado.Count; i++)
			{
				unEmpleado = listaempleado.Item(i);

				string codigo = unEmpleado.SelectSingleNode("codigo").InnerText;
				string nombre = unEmpleado.SelectSingleNode("nombre").InnerText;
				string finicio = unEmpleado.SelectSingleNode("fecha_inicio").InnerText;
				string ffin = unEmpleado.SelectSingleNode("fecha_fin").InnerText;
				
				DateTime f = DateTime.Parse(ffin);
				DateTime ii = DateTime.Parse(finicio);

				int p = Int32.Parse(codigo);

				SqlDataAdapter sqlDa = new SqlDataAdapter("INSERT INTO ListaPrecio values(" + p + ",'" + nombre + "','"+ii+"','"+f+"');", sqlCon);
				
				DataTable dtbl = new DataTable();
				
				sqlDa.Fill(dtbl);
				sqlCon.Close();

				XmlNodeList lista = unEmpleado.SelectNodes("detalle/item");
				XmlNode items;
				///empleados
				for (int j = 0; j < lista.Count; j++)
				{
					items = lista.Item(j);

					string codigop = items.SelectSingleNode("codigo_producto").InnerText;
					string nombres = items.SelectSingleNode("valor").InnerText;
					int producto = Int32.Parse(codigop);
					double precio = double.Parse(nombres);

					SqlDataAdapter sqlD = new SqlDataAdapter("INSERT INTO DetalleLista values(" + codigo + "," + producto + "," + precio + ")", sqlCon);

					DataTable dtb = new DataTable();

					sqlD.Fill(dtb);
					sqlCon.Close();

				}


			}
		}

		


		public void listacliente()
		{
			XmlDocument doc = new XmlDocument();

			doc.Load(archivo.Text);
			XmlNodeList listaempleado = doc.SelectNodes("definicion/lstXCliente");
			XmlNode unEmpleado;
			///empleados
			for (int i = 0; i < listaempleado.Count; i++)
			{
				unEmpleado = listaempleado.Item(i);

				string codigop = unEmpleado.SelectSingleNode("cliente").InnerText;
				string lista = unEmpleado.SelectSingleNode("codigo_lista").InnerText;
				int producto = Int32.Parse(lista);

				SqlDataAdapter sqlDa = new SqlDataAdapter("INSERT INTO ListaCliente values('" + codigop + "'," + producto + ")", sqlCon);

				DataTable dtbl = new DataTable();

				sqlDa.Fill(dtbl);
				sqlCon.Close();

			}

		}



		
	}
}